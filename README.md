\page quick_start

Quick start Guide 
================

Obtain RMT
==========

To obtain a copy of the RMT repository, change into the directory which will house the repository and type:

    git clone https://gitlab.com/Uk-amor/RMT/rmt

Upon successful download you will see the line:

    Checking out files: 100% (/), done

and the new directory `rmt` will appear in your current directory.


Compile RMT
===========

Prerequisites
-------------

Before attempting to compile the RMT code, you will need to have access to the following:

1. A parallel FORTRAN compiler
2. A C compiler
3. CMake version 3.0 or higher
4. LAPACK and BLAS libraries

Compile Instructions
--------------------

Firstly, set up the compilation environment. Type:

    export CC=$(which mpicc)
    export CXX=$(which mpicxx)
    export FC=$(which mpif90)

All three compilers should be from the same suite (eg Intel).

The build structure of RMT is managed with cmake. To compile RMT create a new directory
`build` and call CMake:

    cd rmt
    mkdir build
    cd build
    cmake ../source
    make 

Upon successful compilation, the main executable `rmt.x` will reside in `rmt/build/bin`.


Run RMT
-------

To check that your version of RMT has compiled correctly you may wish to run a few tests.
In the `tests` directory a variety of small and large, atomic and molecular input files 
and expected outputs are available. To test your RMT build, execute, for example:

    cd tests/atomic_tests/small_tests/Ne_4cores
    mkdir test
    cd test
    ln -s ../inputs/* .
    mpiexec -n 4 ~/<path_to_repo>/rmt/build/bin/rmt.x > log.out

You can now check the agreement between the output files in the `test` directory with the
expected ouptuts in `rmt_output`, either by eye or by using the utility script 
`compare_rmt_runs.py`, which can be found in `rmt/utilities/py_lib`.  

\defgroup source
