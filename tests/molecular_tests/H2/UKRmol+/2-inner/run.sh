#!/bin/bash

# Calculate eigenstates of the target (hydrogen ion) using the enhanced
# B-spline basis. Calculate dipole transition moments between the target
# bound states. Then, calculate also eigenstates of the neutral molecule.

echo "H₂⁺: congen"

congen < inputs/target.congen.inp > target.congen.out || exit

mv fort.710 target-configurations.dat

echo "H₂⁺: scatci"

ln -s ../1-integrals/moints fort.16
ln -s target-configurations.dat fort.710

scatci < inputs/target.scatci.inp 1> target.scatci.out 2> target.scatci.err || exit

mv fort.25 target-ci-coeffs.dat
mv fort.26 target-hamiltonian.dat
rm -f fort.710 fort.16

echo "H₂⁺: denprop"

ln -s ../1-integrals/moints fort.17
ln -s target-ci-coeffs.dat fort.25
ln -s target-configurations.dat fort.710

denprop < inputs/target.denprop.inp > target.denprop.out || exit

mv fort.20 target-denprop-fort20.dat
mv fort.21 target-denprop-fort21.dat
mv fort.24 target-properties.dat
mv fort.30 target-denprop-fort30.dat
mv fort.60 target-denprop-fort60.dat
mv fort.61 target-denprop-fort61.dat
mv fort.172 target-denprop-fort172.dat
rm -f fort.17 fort.25 fort.710

for i in $(seq 1 8)
do

    echo "H₂⁰ (sym $i): congen"

    congen < inputs/scattering.congen.$i.inp > scattering.congen.$i.out || exit

    mv fort.$(( 69 + $i )) scattering-configurations-$i.dat

    echo "H₂⁰ (sym $i): scatci"

    ln -s ../1-integrals/moints fort.16
    ln -s target-ci-coeffs.dat fort.266
    ln -s scattering-configurations-$i.dat fort.$(( 69 + $i ))

    scatci < inputs/scattering.scatci.$i.inp 1> scattering.scatci.$i.out 2> scattering.scatci.$i.err || exit

    mv fort.2001 scattering-target-vectors-$i.dat
    rm -f fort.16 fort.$(( 69 + $i )) fort.266

done

mv fort.25 scattering-ci-coeffs.dat
mv fort.26 scattering-hamiltonian.dat
