#!/bin/bash

# Generate MOLDEN file with the Gaussian molecular orbitals
# for the ionized peroxide molecule. See the directory "outputs"
# for sample output files.

psi4 --input inputs/h2o2+.inp --output outputs/h2o2+.out
