#!/bin/bash

# Calculate eigenstates of the target (peroxide molecule) using the standard
# Gaussian basis. Calculate dipole transition moments between the target
# bound states. Then, calculate also eigenstates of the neutral molecule.

C2=(A B)

rm -f *.dat fort.* borndat log_file.* molecule.polarisability *.err *.out

for i in 0 1
do

    echo "H2O2+: congen (sym ${C2[$i]})"

    congen < inputs/target.congen.doublet.${C2[$i]}.inp \
                 1> target.congen.doublet.${C2[$i]}.out \
                 2> target.congen.doublet.${C2[$i]}.err || exit 1

    mv fort.$(( 720 + $i )) target-configurations-${C2[$i]}.dat

    echo "H2O2+: scatci (sym ${C2[$i]})"

    ln -s ../1-integrals/moints fort.16
    ln -s target-configurations-${C2[$i]}.dat fort.72$i

    scatci < inputs/target.scatci.doublet.${C2[$i]}.inp \
                 1> target.scatci.doublet.${C2[$i]}.out \
                 2> target.scatci.doublet.${C2[$i]}.err || exit 1

    rm -f fort.16 fort.72$i fort.82$i

done

mv fort.26 target-ci-coeffs.dat

echo "H2O2+: denprop"

ln -s ../1-integrals/moints fort.17
ln -s target-ci-coeffs.dat fort.26
ln -s target-configurations-A.dat fort.720
ln -s target-configurations-B.dat fort.721

denprop < inputs/target.denprop.inp \
              1> target.denprop.out \
              2> target.denprop.err || exit 1

mv fort.24 target-properties.dat
rm -f fort.17 fort.26 fort.720 fort.721 fort.722

for i in 0 1
do

    echo "H2O2: congen (sym ${C2[$i]})"

    congen < inputs/scattering.congen.singlet.${C2[$i]}.inp \
                 1> scattering.congen.singlet.${C2[$i]}.out \
                 2> scattering.congen.singlet.${C2[$i]}.err || exit 1

    mv fort.7$i scattering-configurations-${C2[$i]}.dat

    echo "H2O2: scatci (sym ${C2[$i]})"

    ln -s ../1-integrals/moints fort.16
    ln -s target-ci-coeffs.dat fort.26
    ln -s scattering-configurations-${C2[$i]}.dat fort.7$i

    scatci < inputs/scattering.scatci.singlet.${C2[$i]}.inp \
                 1> scattering.scatci.singlet.${C2[$i]}.out \
                 2> scattering.scatci.singlet.${C2[$i]}.err || exit 1

    mv fort.2001 scattering-target-vectors-${C2[$i]}.dat
    rm -f fort.16 fort.26 fort.7$i fort.8$i

done

mv fort.25 scattering-ci-coeffs.dat
