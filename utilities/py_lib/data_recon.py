## @namespace data_recon
# @ingroup utilities
#Utility for reconstructing population data files from RMT binary output. If RMT
#is executed with the run time option `binary_data_files = .true.` then the data
#populations are written to a single binary output file `data/popchn.*`. To
#reconstruct this data in post processing the
#utility should be invoked from the command line as
#
#>>> cd data
#>>> python3 ./data_recon.py 
#
#where data is the output directory for the RMT population files. On exit the
#population files are named for their channel ID exactly as if they had been
#written by the RMT code itself. 
#
# @author ACB 28/09/2018


##################################################
from __future__ import print_function
import numpy as np
import glob
from sys import exit

##The first three elements of the popchn file are the header info:
#<number of channels>
#<number of data points per checkpoint> 
#<number of checkpoints>. 
#These three are read in and used to construct the datatype array dt which
#allows the remaining data to be read 
def read_data_info(fname):
    try:
        data_info= np.fromfile(fname,dtype=np.int32,count=4)
    except FileNotFoundError as e:
        print('Not able to read the file {}'.format(fname))
        raise e

    if data_info[3] > 1:
        print ('data_recon is no currently compatible with multiple solution output')
        exit()

    cnt = (data_info[0]+1) * data_info[1] * data_info[2] * data_info[3]

    dt=[]
    dt.extend([('',np.int32) for ii in range(4) ])
    dt.extend([('',np.float64) for ii in range(cnt)])

    return data_info, dt

 
##Read the raw data from the file. The data is organised as follows
#<header information>
#<time values for checkpoint 1>
#<population in each channel L for checkpoint 1>
#<time values for checkpoint 2>
#<population in each channel L for checkpoint 2>
#...
#
#The entire block of data is read in, the header information is stripped out,
#and then the data is reshaped into a 3-d array 
#(checkpt_id, channel_id, time_value)     
def read_pop_data(dt, data_info,fname):
    try:
        data = np.fromfile(fname, dtype=dt)
    except FileNotFoundError as e:
        print('Not able to read the file {}'.format(fname))
        raise e

    data = [x for t in data for x in t] # np.reshape / np.ravel ?
    popdata=np.delete(data,[0,1,2,3])

    results = popdata.reshape(data_info[2],data_info[0]+1, data_info[1])

    return results

##reproduce RMT's naming convention for the pop files.
def build_fname(channel_id,popname):
    
    suffix = popname.split('.')[1]

    prefix = ['popL0','popL','ppL','pL','p']
    sel = len(str(channel_id)) - 1

    return prefix[sel]+str(channel_id)+"."+suffix

##Output populations into separate, formatted files for each channel.
def write_files(results,data_info,popname):
    
    times = []
    for checkpt in range(data_info[2]): # number of checkpoints
        times.extend(results[checkpt][0][:])

    for chan in range(1,data_info[0]+1): # number of channels
        chandat=[]
        for checkpt in range(data_info[2]):
            chandat.extend(results[checkpt][chan][:])
                
        fname = build_fname(chan,popname)
        try:
            with open(fname, 'w+') as opf:
                for tim,dat in zip(times,chandat):
                    opf.write("{:.15E} {:.15E}\n".format(tim,dat))
        except Exception as e:
            print('unable to write to file {}'.format(fname))
            raise e


fnames = glob.glob("popchn.*")
if len(fnames) != 1:
    raise ValueError('one popchn.* file should have been found, found {}'.format(len(fnames)))
else:
    fname = fnames[0]

data_info, dt = read_data_info(fname)
results=read_pop_data(dt,data_info,fname)
write_files(results,data_info,fname)

