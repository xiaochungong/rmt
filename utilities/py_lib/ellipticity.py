"""
Utility for generating the ellipticity from RMT output. We use equation 5 from Antoine et. al. Polarization
of high-order harmonics (PRA 55, 2 page 1314) to calculate chi, from which ellipticity=tan(chi). The utility
should be invoked from the command line as

>>> python3 ./ellipticity.py [-xyz] [-h] <filelist>

Options
=======
-xyz    :  The propagation direction of the pulse (i.e. -x will compute ellipticity in the y/z plane). DEFAULT: compute z only
-h      :  print a help message

Arguments
=========

<filelist> :  is a list of directories containing the RMT output.

specifically the expec_z_all.* and/or expec_v_all* files are required.

Can be invoked for the current directory using

>>> python3 ./ellipticity.py [-xyz] [-h] .

The result will be new files in the RMT directory named ellipticity<gauge><component>
where <gauge> can be either "len" or "vel" (calculated from dipole
length/velocity). Component will be x y or z as requested.

If either the expec_z_all.* or expec_v_all.* RMT output files are missing, then
the corresponding phase will not be computed. If both are missing, or if any
errors are encountered, the code will print an error message.

ACB 20/03/2019
JW 26/09/2019
"""
########################################
from __future__ import print_function
import os
import numpy as np
from multiprocessing import Pool, cpu_count
import filehand as fh
import datahand as dh

def do_conversion(dirname):
    """
    attempts to peform the calculation of the fourier transform for the dipole
    expectation files in a given directory. If this fails, will exit gracefully
    and print an error message to screen.
    """
# get which components of dipole are desired
    options = fh.get_options()
    plane_dims = np.zeros(2)

    solutions_chosen_is_range = True if options['solutions_end'] is not None else False
    if solutions_chosen_is_range:
        solutions = range(options['solutions_start'],options['solutions_end']+1)
    else:
        solutions = range(options['solutions_start'],options['solutions_start']+1)

    try:
        os.chdir(dirname) # with pandas, don't need this - leaving for now

        for sol_id in solutions:

            for name,gauge in zip(['len','vel'],["z","v"]):
                for desired,dim_name,dim_id in zip(options["componentlist"],['x','y','z'],[1,2,3]):
                    harm_data_to_keep = []
                    phase_data_to_keep = []
                    
                    if desired: # if we have been asked to compute this component
                        if dim_id==1:
                            plane_dims[0] = 2
                            plane_dims[1] = 3
                        if dim_id==2:
                            plane_dims[0] = 1
                            plane_dims[1] = 3
                        if dim_id==3:
                            plane_dims[0] = 2
                            plane_dims[1] = 1

                        continue_with_calculation=True
                        for i in [0,1]:
                            working_dim = int(plane_dims[i])
                            try:
                                dt,data,field = fh.get_data_and_field(gauge, working_dim, working_dim, sol_id)
                            except Exception as e:
                                print("could not read data for field polarisation direction: {} in {} {}".format(['x','y','z'][int(working_dim)-1], name, gauge))
                                continue_with_calculation = False
                                raise e # this removes need for 'if continue_with_calc' checks
                                pass
                                
                            
                            if (continue_with_calculation):
                                print("Calculating phase and field strength for field propagation in {} direction. Field polarisation direction:{}".format(dim_name, ['x','y','z'][int(working_dim)-1]))
                                #Calculate HHG for working_dim
                                freqs,harm_data,smoothed = dh.get_hhg_spect(dt, data, gauge, pad_factor=options['pad_factor'])
                                harm_data_to_keep.append(harm_data[1:])

                                #Calculate Phase for working_dim
                                freqs,phase = dh.make_phase_data(data, gauge, dt, stride=1, pad_factor=options['pad_factor'])
                                phase_data_to_keep.append(phase)
                                
                        if (continue_with_calculation):
                            #Calculate gamma (arctan of ratio of harmonic field components)
                            harm_data_to_keep[1]=[nx+0.000000000000000001 for nx in harm_data_to_keep[1]]
                            numer=np.sqrt(np.abs(harm_data_to_keep[0]))
                            denom=np.sqrt(np.abs(harm_data_to_keep[1]))
                            gamma=np.arctan2(numer,denom)
    
                            #Calculate delta (relative phase of harmonic field components)
                            delta=np.zeros(len(phase_data_to_keep[0][:]))
                            for ii in range(len(phase_data_to_keep[0][:])):
                                delta[ii] = phase_data_to_keep[0][ii]-phase_data_to_keep[1][ii]
                            
                            #Calculate ellipticity
                            ellipticity = np.tan(0.5*np.arcsin(np.sin(2.0*gamma)*np.sin(delta)))
                            if options["filterharmonicpeaks"]:
                                ellipticity = dh.filterbyomega(freqs,ellipticity,options["omega"])
                            fh.write_xy_output(freqs,ellipticity,'ellipticity_'+name+'_{0:04d}_'.format(sol_id)+dim_name)

                        
                        if options["mask"]:
                            print('Mask option not implemented for ellipticity calculations. Output will contain alltrajectories')
                        if options["masklong"]:
                            print('Long trajectory mask option not implemented for ellipticity calculations. Output will contain alltrajectories')
    except Exception as e:
        
        print("could not convert " + dirname)
        raise e

################################################################################


# get list of directories
filelist = fh.get_filelist()
# use the p.map parallelisation to convert the listed directories in parallel.
p = Pool(cpu_count())
p.map(do_conversion,filelist)
