
=====================================================
RMatrixI for RMT Readme
=====================================================

The files in this directory are included to enable the calculation of atomic data for RMT using the RMatrixI package. The instructions
conatined within this file are probably sufficient for most small atoms, however some problems might arise when data for larger atoms 
is required. Jack Wragg (jack.wragg@qub.ac.uk) or Connor Ballance (c.ballance@qub.ac.uk) can be contacted for further help.

The following files are contained in this directory:
        1. adas803.pl - the perl script which runs the executables to create the data
        2. currentsite - installation parameters for the executables. At the time of uploading, configured for compilation with ifort 
                         on aigis. As aigis is upgraded this file might need to be modified.
        2. currentsite_gfortran - backup installation parameters, configured for compilation with gfortran. To compile with gfortran,  
                         open the file and fill in the $(BASE) field. Then overwrite the currentsite with this file. The make script
                         should now work as expected.
        3. Makefile - to download and compile the RMatrixI codes
        4. input.dat - an example input file for a helium target

----------------------------------------------
To install RMatrixI on aigis
----------------------------------------------

- Open a terminal window in the Atomic_Data_Files folder
- Run the following commands in this order
        1. make directories
        2. make update_utility (there will probably be an "Error 1" when this completes - ignore this)
        3. make update_serial
        4. make update_parallel
        5. make utility
        6. make serial
        7. make parallel

The executables should now be located in the "AtomicData/bin" folder in the utilities directory.


--------------------------------------------------------------
To Run Script to Calculate Atomic data for RMT on aigis
--------------------------------------------------------------
- copy adas803.pl to the folder where you wish to create the data
- copy parallel_procfile from ./utility/AtomicData/bin/parallel_procfile to folder where you wish to create the data
- create an input file for the data you want to make (instructions for this below, but an example is provided in utlities/Atomic_Data_Files/input.dat)
- run the following command from the folder you want the data to be created in:
        perl -rmt -proc=parallel_procfile <input_file> <Z>
  where <input_file> is the name of the input file, and <Z> is the atomic number of the atom.
- The script will make a folder called 'RMTData', in which the RMatrixI format RMT data should now exist.

----------------------------------------------
Input File Format
----------------------------------------------
To modify the example input for the specific atom, we need to modify the "CONFIGURATION LIST" section, and the "SCALING PARAMETERS" section as follows:

1. Make sure all desired target configurations are listed in the configuration list. Each new configuration should be given a new line, and given in the format nlm, where n and l indicate the orbital, and m is how many electron to include within this orbital. For example, "2p4" would indicate that the configuration contains four 2p electrons. If an orbital is empty it should be omitted, e.g. "1s2 2p1". rather than "1s2 2s0 2p1", which will cause an error.

2. Make sure all orbitals used in the configurations above are listed within the "SCALING PARAMETERS" section, followed by a " = 1.0" in the form "nl = 1.0" where nl indicates the orbital. For example "4d = 1.0" would be the entry for the 4d orbital. Each new entry should be on a new line.

3. The "GENERAL" section can mostly be ignored for rmt, apart from the following options:
        -2Jmax_ex         : Double the maximum L or J
        -maxc             : Approximate number of inner region continuum orbitals (i.e. NRANG2)
        -rmt_boundary     : boundary between inner and outer region
        -rmt_spline_order : order of splines
