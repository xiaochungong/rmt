! Copyright 2018 
!
! Greg Armstrong, Jakub Benda, Andrew Brown, Daniel Clarke, Michael Lysaght,
! Zdenek Masin, Robert McGibbon, Laura Moore, Jonathan Parker, Martin Plummer,
! Ken Taylor, Hugo van der Hart, Jack Wragg                                    
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in trunk/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> main program file for the GetProcInfo program.
PROGRAM GetProcInfo

    USE mpi

    USE tdse_dependencies
    USE initial_conditions,     ONLY: no_of_pes_to_use_inner,           &
                                      no_of_pes_to_use_outer
    USE readhd,                 ONLY: LML_block_lrgl,LML_block_npty,    &
                                      LML_block_ml
    USE mpi_layer_lblocks,      ONLY: my_L_block_id => my_LML_block_id,Lb_rank
    USE distribute_hd_blocks2,  ONLY: numrows, rowend, rowbeg
    USE mpi_communications,     ONLY: I_am_in_Inner_Region,             &
                                      i_am_in_outer_region,             &
                                      i_am_inner_master,                &
                                      mpi_comm_region,                  &
                                      get_my_group_pe_id

    USE grid_parameters,        ONLY: x_1st, &
                                      x_last, &
                                      channel_id_1st, &
                                      channel_id_last

    IMPLICIT NONE

    REAL(wp)   :: r_at_region_bndry
    INTEGER    :: number_channels
    REAL(wp)   :: Z_minus_N

    !-----------------------------------------------------------------------
    ! Variables for breaking integration up into stages, and for initializing
    ! Psi.
    !-----------------------------------------------------------------------
    INTEGER    :: start_of_run_timeindex
    INTEGER    :: stage
    INTEGER    :: my_post

    Integer,allocatable :: L_To_Write(:)
    Integer,allocatable :: LM(:)
    Integer,allocatable :: Parity(:)
    Integer,allocatable :: LBlock_To_Write(:)
    Integer,allocatable :: LBlock_Rank_To_Write(:)
    Integer,allocatable :: rowbeg_To_Write(:)
    Integer,allocatable :: rowend_To_Write(:)
    Integer,allocatable :: numrows_To_Write(:)
    Integer,allocatable :: L_Block_Base(:)

    Integer,allocatable :: Outer_Region_X_1st(:)
    Integer,allocatable :: Outer_Region_X_Last(:)
    Integer,allocatable :: Outer_Region_Channel_1st(:)
    Integer,allocatable :: Outer_Region_Channel_Last(:)

    INTEGER :: Inner_Core_Counter
    INTEGER :: Outer_Core_Counter
    INTEGER :: ierror
    INTEGER :: status(MPI_STATUS_SIZE)
    INTEGER :: my_pe_id

!                           ___      _ _
!                          |_ _|_ _ (_) |_
!                           | || ' \| |  _|
!                          |___|_||_|_|\__|

    CALL initialise_everything(r_at_region_bndry, &
                               Z_minus_N, &
                               number_channels, &
                               my_post, &
                               start_of_run_timeindex, &
                               stage)

    Allocate(L_To_Write(no_of_pes_to_use_inner))
    Allocate(LM(no_of_pes_to_use_inner))
    Allocate(Parity(no_of_pes_to_use_inner))
    Allocate(LBlock_To_Write(no_of_pes_to_use_inner))
    Allocate(LBlock_Rank_To_Write(no_of_pes_to_use_inner))
    Allocate(rowbeg_To_Write(no_of_pes_to_use_inner))
    Allocate(rowend_To_Write(no_of_pes_to_use_inner))
    Allocate(numrows_To_Write(no_of_pes_to_use_inner))
    Allocate(L_Block_Base(no_of_pes_to_use_inner))
    
    Allocate(Outer_Region_X_1st(no_of_pes_to_use_outer))
    Allocate(Outer_Region_X_Last(no_of_pes_to_use_outer))
    Allocate(Outer_Region_Channel_1st(no_of_pes_to_use_outer))
    Allocate(Outer_Region_Channel_Last(no_of_pes_to_use_outer))

    L_To_Write=0
    LM=0
    Parity=0
    LBlock_To_Write=0
    LBlock_Rank_To_Write=0
    rowbeg_To_Write=0
    rowend_To_Write=0
    numrows_To_Write=0
    L_Block_Base=0
    Outer_Region_X_1st=0
    Outer_Region_X_Last=0
    Outer_Region_Channel_1st=0
    Outer_Region_Channel_Last=0

    Call get_my_group_pe_id(my_pe_id)

    IF (I_am_in_Inner_Region) Then

        IF (I_am_Inner_Master) Then

            L_To_Write(1)=LML_block_lrgl(my_L_block_id)
            LM(1)=LML_block_ml(my_L_block_id)
            Parity(1)=LML_block_npty(my_L_block_id)
            LBlock_To_Write(1)=my_L_block_id
            LBlock_Rank_To_Write(1)=Lb_rank
            rowbeg_To_Write(1)=rowbeg
            rowend_To_Write(1)=rowend
            numrows_To_Write(1)=numrows
            L_Block_Base(1)=my_L_block_id



            Do Inner_Core_Counter=2,no_of_pes_to_use_inner

                Call MPI_Barrier(mpi_comm_region,ierror)

                Call MPI_RECV(L_To_Write(Inner_Core_Counter), 1,MPI_INTEGER,Inner_Core_Counter-1,1,&
                mpi_comm_region, status, ierror)
                Call MPI_RECV(LM(Inner_Core_Counter), 1,MPI_INTEGER,Inner_Core_Counter-1,2,&
                mpi_comm_region, status, ierror)
                Call MPI_RECV(Parity(Inner_Core_Counter), 1,MPI_INTEGER,Inner_Core_Counter-1,3,&
                mpi_comm_region, status, ierror)
                Call MPI_RECV(LBlock_To_Write(Inner_Core_Counter), 1,MPI_INTEGER,Inner_Core_Counter-1,4,&
                mpi_comm_region, status, ierror)
                Call MPI_RECV(LBlock_Rank_To_Write(Inner_Core_Counter), 1,MPI_INTEGER,Inner_Core_Counter-1,5,&
                mpi_comm_region, status, ierror)
                Call MPI_RECV(rowbeg_To_Write(Inner_Core_Counter), 1,MPI_INTEGER,Inner_Core_Counter-1,6,&
                mpi_comm_region, status, ierror)
                Call MPI_RECV(rowend_To_Write(Inner_Core_Counter), 1,MPI_INTEGER,Inner_Core_Counter-1,7,&
                mpi_comm_region, status, ierror)
                Call MPI_RECV(numrows_To_Write(Inner_Core_Counter), 1,MPI_INTEGER,Inner_Core_Counter-1,8,&
                mpi_comm_region, status, ierror)

                if (LBlock_To_Write(Inner_Core_Counter).eq.LBlock_To_Write(Inner_Core_Counter-1)) Then
                    L_Block_Base(Inner_Core_Counter)=L_Block_Base(Inner_Core_Counter-1)
                ELSE
                    L_Block_Base(Inner_Core_Counter)=Inner_Core_Counter
                END IF
            END DO

        ELSE

            Do Inner_Core_Counter=2,my_pe_id+1
                Call MPI_Barrier(mpi_comm_region,ierror)
            End Do

            Call MPI_SEND(LML_block_lrgl(my_L_block_id), 1,MPI_INTEGER,0,1,&
            mpi_comm_region, ierror)
            Call MPI_SEND(LML_block_ml(my_L_block_id), 1,MPI_INTEGER,0,2,&
            mpi_comm_region, ierror)
            Call MPI_SEND(LML_block_npty(my_L_block_id), 1,MPI_INTEGER,0,3,&
            mpi_comm_region, ierror)
            Call MPI_SEND(my_L_block_id, 1,MPI_INTEGER,0,4,&
            mpi_comm_region, ierror)
            Call MPI_SEND(Lb_rank, 1,MPI_INTEGER,0,5,&
            mpi_comm_region, ierror)
            Call MPI_SEND(rowbeg, 1,MPI_INTEGER,0,6,&
            mpi_comm_region, ierror)
            Call MPI_SEND(rowend, 1,MPI_INTEGER,0,7,&
            mpi_comm_region, ierror)
            Call MPI_SEND(numrows, 1,MPI_INTEGER,0,8,&
            mpi_comm_region, ierror)

            Do Inner_Core_Counter=my_pe_id+2,no_of_pes_to_use_inner
                Call MPI_Barrier(mpi_comm_region,ierror)
            End Do
        END IF

        IF (I_am_Inner_Master) Then
            OPEN(unit=20,file='RMTProcInfo',form='formatted')

            Write(20,*) 'Number of inner region Cores:'
            Write(20,*) no_of_pes_to_use_inner
            Write(20,*) ''
            Write(20,*) 'Number of outer region Cores:'
            Write(20,*) no_of_pes_to_use_outer
            Write(20,*) ''
            Write(20,fmt='(A90)') '     PE_ID         L        ML        Pi    LBLock     LRank   ' & 
                 // 'rowbeg    rowend   numrows'
            Write(20,fmt='(A90)') '---------------------------------------------------------------' &
                 // '------------------------------------------'
            Do Inner_Core_Counter=1,no_of_pes_to_use_inner
                Write(20,fmt='(9I10)') Inner_Core_Counter,       &
                        L_To_Write(L_Block_Base(Inner_Core_Counter)),       &
                        LM(L_Block_Base(Inner_Core_Counter)),       &
                        Parity(L_Block_Base(Inner_Core_Counter)),       &
                        LBlock_To_Write(Inner_Core_Counter),       &
                        LBlock_Rank_To_Write(Inner_Core_Counter),       &
                        rowbeg_To_Write(Inner_Core_Counter),       &
                        rowend_To_Write(Inner_Core_Counter),       &
                        numrows_To_Write(Inner_Core_Counter)
            END DO

            DO Outer_Core_Counter=1,no_of_pes_to_use_outer
               Call MPI_RECV(Outer_Region_X_1st(Outer_Core_Counter), 1,MPI_INTEGER,    &
                    no_of_pes_to_use_inner+Outer_Core_Counter-1,0,mpi_comm_world, status, ierror)
               Call MPI_RECV(Outer_Region_X_Last(Outer_Core_Counter), 1,MPI_INTEGER,   &
                    no_of_pes_to_use_inner+Outer_Core_Counter-1,0,mpi_comm_world, status, ierror)
                Call MPI_RECV(Outer_Region_Channel_1st(Outer_Core_Counter), 1,MPI_INTEGER,   &
                     no_of_pes_to_use_inner+Outer_Core_Counter-1,0,mpi_comm_world, status, ierror)
                Call MPI_RECV(Outer_Region_Channel_Last(Outer_Core_Counter), 1,MPI_INTEGER,  &
                     no_of_pes_to_use_inner+Outer_Core_Counter-1,0,mpi_comm_world, status, ierror)
            END DO

            Write(20,fmt='(A50)')
            Write(20,fmt='(A50)') '     PE_ID     X_1st    X_Last  Chan_1st Chan_Last'    
            Write(20,fmt='(A50)') '--------------------------------------------------------------' &
                 // '-------------------------------------------'
            Do Outer_Core_Counter=1,no_of_pes_to_use_outer
                Write(20,fmt='(9I10)') Outer_Core_Counter,       &
                        Outer_Region_X_1st(Outer_Core_Counter),       &
                        Outer_Region_X_Last(Outer_Core_Counter),       &
                        Outer_Region_Channel_1st(Outer_Core_Counter),       &
                        Outer_Region_Channel_Last(Outer_Core_Counter)
            END DO

            Close (20)

        END IF

    END IF

    IF (i_am_in_outer_region) THEN

        Call MPI_SEND(X_1st, 1,MPI_INTEGER,0,0,&
        mpi_comm_world, ierror)
        Call MPI_SEND(X_Last, 1,MPI_INTEGER,0,0,&
        mpi_comm_world, ierror)
        Call MPI_SEND(channel_id_1st, 1,MPI_INTEGER,0,0,&
        mpi_comm_world, ierror)
        Call MPI_SEND(channel_id_last, 1,MPI_INTEGER,0,0,&
        mpi_comm_world, ierror)

    END IF

    !If I am Outer
        !Open File
        !Write: OuterRank pointbegin pointend
    !If I am Inner
        !Open File
        !Write: InnerRank L LM Parity LBlock Rank rowbeg rowend numrows

    CALL end_program

END PROGRAM GetProcInfo

