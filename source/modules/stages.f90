! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Handles the position within a calculation (time-steps) when a calculation is
!> starting or restarting.

MODULE stages

    USE precisn,                   ONLY: wp
    USE rmt_assert,                ONLY: assert
    USE communications_parameters, ONLY: pe_id_1st
    USE io_routines,               ONLY: get_stage, &
                                         check_status_file_exists
    USE mpi_communications,        ONLY: get_my_pe_id, &
                                         message_1st, &
                                         message_last, &
                                         first_pe_broadcasts_a_message

    IMPLICIT NONE

    INTEGER, PARAMETER, PUBLIC :: stage_first = 1
    INTEGER, PARAMETER, PUBLIC :: timeindex_first = 0
    LOGICAL, SAVE, PUBLIC      :: starting_from_scratch

CONTAINS

    SUBROUTINE broadcast_stage(start_of_run_timeindex, stage)

        INTEGER, INTENT(OUT) :: start_of_run_timeindex, stage
        REAL(wp)             :: message(message_1st:message_last)
        INTEGER              :: my_pe_id

        CALL get_my_pe_id(my_pe_id)

        IF (my_pe_id == pe_id_1st) THEN

            CALL get_stage(timeindex_first, stage_first, start_of_run_timeindex, stage)

        END IF

        ! Send/receive Stage, Start_of_Run_TimeIndex, Final_TimeIndex
        ! Only the first PE has the correct values of Stage, etc.
        ! For the 1st PE these are IN parameters.
        ! For the other PE's these are OUT parameters.

        message = 0.0_wp
        message(1) = REAL(stage, wp)
        message(2) = REAL(start_of_run_timeindex, wp)

        CALL first_pe_broadcasts_a_message(message)

        stage = NINT(message(1))
        start_of_run_timeindex = NINT(message(2))

        IF (stage .EQ. stage_first) THEN
            starting_from_scratch = .true.
        ELSE
            starting_from_scratch = .false.
        END IF

    END SUBROUTINE broadcast_stage

!---------------------------------------------------------------------------

    SUBROUTINE init_stages
    ! Check File Containing Stage Info exists

        INTEGER    :: my_pe_id
        LOGICAL    :: status_file_exists
        REAL(wp)   :: message(message_1st:message_last)
        INTEGER    :: file_exists_flag

        message = 0.0_wp
        message(1) = REAL(0.0, wp)
        CALL get_my_pe_id(my_pe_id)

        IF (my_pe_id == pe_id_1st) THEN
            CALL check_status_file_exists(status_file_exists)
            IF (.NOT. status_file_exists) THEN
                message(1) = REAL(-1.0, wp)
            ELSE
                message(1) = REAL(1.0, wp)
            END IF
        END IF

        CALL first_pe_broadcasts_a_message(message)

        file_exists_flag = NINT(message(1))

        IF (file_exists_flag == -1) THEN
            CALL assert(.false., 'ERROR Status File Does Not Exist for this version: ')
        ELSE IF (file_exists_flag == 1) THEN

        ELSE
            CALL assert(.false., 'ERROR Status File Does Not Exist for this version: ')
        END IF

    END SUBROUTINE init_stages

END MODULE stages
