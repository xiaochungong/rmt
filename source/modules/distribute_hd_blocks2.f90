! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!
!> @ingroup source
!> @brief Handles the distribution of the D file data
!> within each symmetry block. Each block has its own master which has already
!> received all the data from the inner region master (in
!> distribute_hd_blocks.)

MODULE distribute_hd_blocks2

    USE precisn,    ONLY: wp
    USE rmt_assert, ONLY: assert
    USE MPI

    IMPLICIT NONE

    INTEGER, SAVE                      :: rowbeg, rowend, numrows
    INTEGER, ALLOCATABLE, SAVE         :: mv_disp(:), mv_counts(:)
    COMPLEX(wp), ALLOCATABLE, SAVE     :: loc_dblock_u(:, :), loc_dblock_d(:, :), &
                                          loc_dblock_s(:, :), loc_surf_amps(:, :)
    COMPLEX(wp), ALLOCATABLE, SAVE     :: loc_vblock_u(:, :), loc_vblock_d(:, :), &
                                          loc_vblock_s(:, :)
    REAL(wp), ALLOCATABLE, SAVE        :: loc_diag_els(:)

    PRIVATE scatter_dblocks_within_Lblock
    PRIVATE get_num_rows_for_local_dblocks
    PRIVATE get_displacements_within_Lblock
    PUBLIC setup_mpi_layer_2
    PUBLIC dealloc_distribute_hd_blocks2
    PUBLIC mv_disp, mv_counts, numrows, rowend, rowbeg
    PUBLIC loc_dblock_u, loc_dblock_d, loc_diag_els, loc_surf_amps
    PUBLIC loc_vblock_u, loc_vblock_d

CONTAINS

    SUBROUTINE setup_mpi_layer_2

        USE distribute_hd_blocks, ONLY: deallocate_my_dblocks, &
                                        deallocate_my_diag_els

        ! Each core calculates the number of rows it will handle within the L block
        CALL get_num_rows_for_local_dblocks

        ! Keep track of which rows are handled by each core
        CALL get_displacements_within_Lblock

        ! Masters of each L Block distribute within their L block
        CALL scatter_dblocks_within_Lblock

        ! Now deallocate arrays that span the whole L block
        CALL deallocate_my_dblocks
        CALL deallocate_my_diag_els

    END SUBROUTINE setup_mpi_layer_2

!-----------------------------------------------------------------------------

    !> \brief   Distribute Hamiltonian blocks within the block group
    !>
    !> The block master will send portions of the free-field and dipole Hamiltonian blocks and surface amplitudes
    !> to processes within the block group. The subroutine makes use of the MPI_Type_create_subarray call to declare
    !> a subset of the dipole matrix that is to be sent to a given rank. Then, MPI_Alltoallw is a group-collective call
    !> that dispatches all such sub-blocks to their final recipients in one go. This way, each process receives only
    !> the row interval defined by fields \ref mv_disp and \ref mv_counts, extracted from the original (column-major) matrix.
    !>
    SUBROUTINE scatter_dblocks_within_Lblock

        USE distribute_hd_blocks, ONLY: my_dblock_u, my_dblock_d, my_dblock_s, &
                                        my_vblock_u, my_vblock_d, my_vblock_s, &
                                        my_surf_amps, &
                                        my_diag_els
        USE initial_conditions,   ONLY: dipole_velocity_output
        USE mpi_layer_lblocks,    ONLY: Lb_comm, Lb_size, i_am_block_master, my_LML_block_id
        USE readhd,               ONLY: max_L_block_size, &
                                        LML_block_tot_nchan, &
                                        dipole_coupled, &
                                        downcoupling, samecoupling, upcoupling

        REAL(wp), ALLOCATABLE :: re_surf_amps(:,:)
        INTEGER,  ALLOCATABLE :: sendtypes(:), sendcounts(:), displacmnt(:), recvtypes(:), recvcounts(:)

        INTEGER :: ns, nc, rank, ierr
        LOGICAL :: d, s, u

        ns = max_L_block_size
        nc = LML_block_tot_nchan

        d = ANY(IAND(dipole_coupled(my_LML_block_id, :), downcoupling) /= 0)
        s = ANY(IAND(dipole_coupled(my_LML_block_id, :), samecoupling) /= 0)
        u = ANY(IAND(dipole_coupled(my_LML_block_id, :), upcoupling)   /= 0)

        ALLOCATE (loc_diag_els(numrows), loc_surf_amps(numrows, nc), re_surf_amps(numrows, nc), &
                  sendtypes(0:Lb_size-1), displacmnt(0:Lb_size-1), &
                  recvtypes(0:Lb_size-1), sendcounts(0:Lb_size-1), recvcounts(0:Lb_size-1), stat = ierr)

        IF (d) THEN
            ALLOCATE (loc_dblock_d(numrows, ns), stat = ierr)
            CALL assert(ierr == 0, 'Failed to allocate dipole block in scatter_dblocks_within_Lblock')
            IF (dipole_velocity_output) THEN
                ALLOCATE (loc_vblock_d(numrows, ns), stat = ierr)
                CALL assert(ierr == 0, 'Failed to allocate dipole block in scatter_dblocks_within_Lblock')
            END IF
        END IF

        IF (s) THEN
            ALLOCATE (loc_dblock_s(numrows, ns), stat = ierr)
            CALL assert(ierr == 0, 'Failed to allocate dipole block in scatter_dblocks_within_Lblock')
            IF (dipole_velocity_output) THEN
                ALLOCATE (loc_vblock_s(numrows, ns), stat = ierr)
                CALL assert(ierr == 0, 'Failed to allocate dipole block in scatter_dblocks_within_Lblock')
            END IF
        END IF

        IF (u) THEN
            ALLOCATE (loc_dblock_u(numrows, ns), stat = ierr)
            CALL assert(ierr == 0, 'Failed to allocate dipole block in scatter_dblocks_within_Lblock')
            IF (dipole_velocity_output) THEN
                ALLOCATE (loc_vblock_u(numrows, ns), stat = ierr)
                CALL assert(ierr == 0, 'Failed to allocate dipole block in scatter_dblocks_within_Lblock')
            END IF
        END IF

        ! distribute diagonal block elements within the L-block group (this is easy)
        CALL MPI_SCATTERV(my_diag_els, mv_counts, mv_disp, MPI_DOUBLE_PRECISION, &
                          loc_diag_els, numrows, MPI_DOUBLE_PRECISION, 0, Lb_comm, ierr)
        CALL assert(ierr == MPI_SUCCESS, 'Eigen-energy scatter failed in scatter_dblocks_within_Lblock.')

        ! how many views to send to / receive from each process, and after which offset they start
        sendcounts(:) = MERGE(1, 0, i_am_block_master)
        recvcounts(:) = 0
        displacmnt(:) = 0

        ! define amplitude sub-block views for each process in the MPI group
        DO rank = 0, Lb_size - 1
            CALL MPI_TYPE_CREATE_SUBARRAY(2, (/ ns, nc /), (/ mv_counts(rank), nc /), (/ mv_disp(rank), 0 /), &
                                          MPI_ORDER_FORTRAN, MPI_DOUBLE_PRECISION, sendtypes(rank), ierr)
            CALL assert(ierr == MPI_SUCCESS, 'Failed to create amplitude subarray type in scatter_dblocks_within_Lblock.')
            CALL MPI_TYPE_COMMIT(sendtypes(rank), ierr)
            CALL assert(ierr == MPI_SUCCESS, 'Failed to commit amplitude subarray type in scatter_dblocks_within_Lblock.')
        END DO

        ! distribute surface amplitudes within the L-block group
        recvtypes(:) = MPI_DOUBLE_PRECISION
        recvcounts(0) = numrows * nc
        loc_surf_amps(:,:) = 0

        CALL MPI_ALLTOALLW(my_surf_amps, sendcounts, displacmnt, sendtypes, &
                           re_surf_amps, recvcounts, displacmnt, recvtypes, Lb_comm, ierr)
        loc_surf_amps(:,:) = re_surf_amps(:,:)

        ! define dipole sub-block views for each process in the MPI group
        DO rank = 0, Lb_size - 1
            CALL MPI_TYPE_FREE(sendtypes(rank), ierr)
            CALL MPI_TYPE_CREATE_SUBARRAY(2, (/ ns, ns /), (/ mv_counts(rank), ns /), (/ mv_disp(rank), 0 /), &
                                          MPI_ORDER_FORTRAN, MPI_DOUBLE_COMPLEX, sendtypes(rank), ierr)
            CALL assert(ierr == MPI_SUCCESS, 'Failed to create dipole subarray type in scatter_dblocks_within_Lblock.')
            CALL MPI_TYPE_COMMIT(sendtypes(rank), ierr)
            CALL assert(ierr == MPI_SUCCESS, 'Failed to commit dipole subarray type in scatter_dblocks_within_Lblock.')
        END DO

        ! distribute length-gauge dipole blocks within the L-block group
        recvtypes(:) = MPI_DOUBLE_COMPLEX
        recvcounts(0) = numrows * ns
        ierr = MPI_SUCCESS

        IF (d) CALL MPI_ALLTOALLW(my_dblock_d, sendcounts, displacmnt, sendtypes, &
                                 loc_dblock_d, recvcounts, displacmnt, recvtypes, Lb_comm, ierr)
        CALL assert(ierr == MPI_SUCCESS, 'All-to-all d-block communication failed in scatter_dblocks_within_Lblock.')
        IF (s) CALL MPI_ALLTOALLW(my_dblock_s, sendcounts, displacmnt, sendtypes, &
                                 loc_dblock_s, recvcounts, displacmnt, recvtypes, Lb_comm, ierr)
        CALL assert(ierr == MPI_SUCCESS, 'All-to-all s-block communication failed in scatter_dblocks_within_Lblock.')
        IF (u) CALL MPI_ALLTOALLW(my_dblock_u, sendcounts, displacmnt, sendtypes, &
                                 loc_dblock_u, recvcounts, displacmnt, recvtypes, Lb_comm, ierr)
        CALL assert(ierr == MPI_SUCCESS, 'All-to-all u-block communication failed in scatter_dblocks_within_Lblock.')

        ! distribute velocity-gauge dipole blocks within the L-block group
        IF (dipole_velocity_output) THEN

            IF (d) CALL MPI_ALLTOALLW(my_vblock_d, sendcounts, displacmnt, sendtypes, &
                                     loc_vblock_d, recvcounts, displacmnt, recvtypes, Lb_comm, ierr)
            CALL assert(ierr == MPI_SUCCESS, 'All-to-all d-block communication failed in scatter_dblocks_within_Lblock (vel).')
            IF (s) CALL MPI_ALLTOALLW(my_vblock_s, sendcounts, displacmnt, sendtypes, &
                                     loc_vblock_s, recvcounts, displacmnt, recvtypes, Lb_comm, ierr)
            CALL assert(ierr == MPI_SUCCESS, 'All-to-all s-block communication failed in scatter_dblocks_within_Lblock (vel).')
            IF (u) CALL MPI_ALLTOALLW(my_vblock_u, sendcounts, displacmnt, sendtypes, &
                                     loc_vblock_u, recvcounts, displacmnt, recvtypes, Lb_comm, ierr)
            CALL assert(ierr == MPI_SUCCESS, 'All-to-all u-block communication failed in scatter_dblocks_within_Lblock (vel).')

        END IF

        ! clean up
        DO rank = 0, Lb_size - 1
            CALL MPI_TYPE_FREE(sendtypes(rank), ierr)
        END DO

    END SUBROUTINE scatter_dblocks_within_Lblock

!-----------------------------------------------------------------------------

    SUBROUTINE get_num_rows_for_local_dblocks

        USE initial_conditions,ONLY: debug
        USE mpi_layer_lblocks, ONLY: inner_region_rank, &
                                     my_LML_block_id, &
                                     Lb_rank, &
                                     Lb_size
        USE readhd,            ONLY: states_per_LML_block

        IMPLICIT NONE

        INTEGER    :: i, L_block_size
        REAL(wp)   :: avrow, rse, rsb

        L_block_size = states_per_LML_block(my_LML_block_id)

        avrow = FLOAT(L_block_size)/FLOAT(Lb_size)

        rsb = 0.0_wp
        rse = 0.0_wp

        DO i = 0, Lb_rank
            rsb = rse
            rse = rsb + avrow
        END DO

        rowbeg = INT(rsb) + 1
        rowend = INT(rse)

        IF (Lb_rank == Lb_size - 1) rowend = L_block_size

        numrows = rowend - rowbeg + 1

        IF (debug) PRINT *, 'in_reg_rank', inner_region_rank, 'rowb', rowbeg, 'rowe', rowend

    END SUBROUTINE get_num_rows_for_local_dblocks

!-----------------------------------------------------------------------------

    SUBROUTINE get_displacements_within_Lblock

        USE mpi_layer_lblocks, ONLY: Lb_comm, &
                                     Lb_size

        IMPLICIT NONE

        INTEGER  :: i, err, ierr

        ALLOCATE (mv_counts(0:Lb_size - 1), mv_disp(0:Lb_size - 1), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with mv_counts and mv_disp')

        mv_counts = 0
        mv_disp = 0

        CALL MPI_ALLGATHER(numrows, 1, MPI_INTEGER, mv_counts, 1, MPI_INTEGER, &
                           Lb_comm, ierr)

        mv_disp(0) = 0

        IF (Lb_size > 1) THEN
            DO i = 1, Lb_size - 1
                mv_disp(i) = mv_counts(i - 1) + mv_disp(i - 1)
            END DO
        END IF

    END SUBROUTINE get_displacements_within_Lblock

!-----------------------------------------------------------------------------

    SUBROUTINE dealloc_distribute_hd_blocks2

        IMPLICIT NONE

        INTEGER   :: err

        DEALLOCATE (mv_disp, mv_counts, loc_diag_els, loc_surf_amps, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error for distribute_hd_blocks2')

        IF (ALLOCATED(loc_dblock_s)) THEN
            DEALLOCATE (loc_dblock_s, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error 2 for distribute_hd_blocks2')
        END IF

        IF (ALLOCATED(loc_dblock_d)) THEN
            DEALLOCATE (loc_dblock_d, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error 3 for distribute_hd_blocks2')
        END IF

        IF (ALLOCATED(loc_dblock_u)) THEN
            DEALLOCATE (loc_dblock_u, stat=err)
            CALL assert(err .EQ. 0, 'deallocation error 4 for distribute_hd_blocks2')
        END IF

    END SUBROUTINE dealloc_distribute_hd_blocks2

END MODULE distribute_hd_blocks2


!> @page inner_layer_2 Inner Region: Layer Two
!!
!! @brief Second layer of parallelisation in the Inner Region
!!
!!
!!#### Layer 2
!!
!!Evidently, there are systems where certain symmetries will contain
!!substantially more states than others. As suggested by the diagram
!!above, in this case the dipole blocks are substantially larger, and the
!!number of multiplications required on each iteration grows with the
!!square of the size of the block. Hence the code has the flexibility to
!!assign multiple MPI tasks to each block. The tasks are allocated by a
!!routine which first assigns one task per block, and then calculates
!!which block has the most work per task, assigning an additional task,
!!until all tasks are allocated. The table below shows a typical
!!allocation of 144 tasks to the 10 blocks used in a calculation for
!!singly ionised neon.
!!
!!   Block |  States |  Tasks
!!  -------| --------| -------
!!     0   |   429   |    7
!!     1   |   970   |   29
!!     2   |   1126  |   37
!!     3   |   979   |   29
!!     4   |   726   |   17
!!     5   |   500   |    9
!!     6   |   369   |    5
!!     7   |   305   |    4
!!     8   |   284   |    4
!!     9   |   276   |    3
!!
!!Each block is now controlled by the so-called ‘block master’ which
!!communicates with the neighbouring blocks and distributes data to the
!!tasks within the block. Each task within a block handles an equal number
!!of rows of the matrix vector multiplication. The relevant portions of
!!the dipole blocks are distributed to each task at the start of the
!!calculation by the block master and the wavefunction on each iteration.
!!Communication within each block is handled on the communicator
!!`Lb_comm`.
!! @image html communicators.png
!! @image latex communicators.png


!> @page inner_layer_3 Inner Region: Layer Three
!!
!! @brief Third layer of parallelisation in the Inner Region
!!
!!
!!#### Layer 3
!!
!!The final layer of parallelism in the inner region is shared memory
!!parallelisation on each MPI task. From layers 1 and 2, each MPI task has
!!a chunk of the Hamiltonian matrix and wavefunction vector with which it
!!performs matrix-vector multiplications. The shared memory parallelism is
!!implemented in two ways.
!!
!!First, several do loop structures with independent loops are farmed out
!!to the shared memory threads with a simple `!$OMP PARALLEL DO`.
!!Secondly, all library routines for linear algebra which support
!!shared-memory processing will execute in parallel on all available
!!shared memory threads.
!!
!!**In practice, we have found only marginal performance gain from using
!!more than one OpenMP thread in the inner region.**



