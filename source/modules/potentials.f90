! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!
!
!> @ingroup source
!> @brief Manages the provision of long-range potential matrix data during the
!> calculation. The matrices are set up in @ref lrpots but accessed here.

MODULE potentials

    USE precisn, ONLY: wp

    IMPLICIT NONE

    PRIVATE
    PUBLIC makewemat, makewpmat, makewpmat2, makewdpmat, &
           makewpmat2_v, makewdpmat_v

CONTAINS

!---------------------------------------------------------------------------
! THE WE POTENTIAL
!---------------------------------------------------------------------------

    SUBROUTINE makewemat(x, ich, jch, wemat)

        USE lrpots, ONLY: cfuu
        USE readhd, ONLY: lamax

        REAL(wp), INTENT(IN)  :: x
        INTEGER, INTENT(IN)   :: ich, jch
        INTEGER               :: lambda
        REAL(wp)              :: rr, sum, rr2
        REAL(wp), INTENT(OUT) :: wemat

!       PRINT*,'lamax=',lamax

        rr = 1._wp/x
        rr2 = rr*rr
        wemat = 0.0_wp
        sum = 0.0_wp

        DO lambda = 1, lamax
!           PRINT*,'cfuu(ich,jch,lambda)=',cfuu(ich,jch,lambda)
!           sum = sum + cfuu(ich,jch,lambda)*rr**(lambda+1)
            sum = sum + cfuu(ich - jch, jch, lambda)*rr2
            rr2 = rr2*rr
        END DO

        ! Note wemat needs to be multiplied by a factor of 0.5 - Hugo
        ! cfuu coefficients are given in Rydbergs, not atomic units

        wemat = 0.50_wp*sum
!       wemat = sum

        ! add on k^2 term here:
!       IF (ich == jch) THEN
!           wemat = wemat - 0.50_wp*ksq(ich)  ! NB ksq defined as -2E, so -0.5 ksq = E
!       END IF

    END SUBROUTINE makewemat

!---------------------------------------------------------------------------

    SUBROUTINE makewpmat(ich, jch, wpmat)

    ! DDAC: Required in velocity gauge

        USE lrpots, ONLY: wp1pot

        INTEGER, INTENT(IN)      :: ich, jch
        COMPLEX(wp), INTENT(OUT) :: wpmat(3)

        INTEGER :: i, n

        n = SIZE(wp1pot, 1)  ! atomic = 1, molecular = 3

        DO i = 1, 3
            wpmat(i) = wp1pot(MIN(i, n), ich-jch, jch)
        END DO

        !wpmat = wp1pot(ich, jch)

    END SUBROUTINE makewpmat

!---------------------------------------------------------------------------

    SUBROUTINE makewpmat2(ich, jch, wpmat2)

    ! DDAC: Required in both length and velocity gauges

        USE lrpots, ONLY: wp2pot

        INTEGER, INTENT(IN)      :: ich, jch
        COMPLEX(wp), INTENT(OUT) :: wpmat2(3)

        INTEGER :: i, n

        n = SIZE(wp2pot, 1)  ! atomic = 1, molecular = 3

        DO i = 1, 3
            wpmat2(i) = wp2pot(MIN(i, n), ich-jch, jch)
        END DO

    END SUBROUTINE makewpmat2

!---------------------------------------------------------------------------

    SUBROUTINE makewpmat2_v(ich, jch, wpmat2_v)

        USE lrpots, ONLY: wp2pot_v

        ! DDAC: Required in velocity gauge
        INTEGER, INTENT(IN)      :: ich, jch
        COMPLEX(wp), INTENT(OUT) :: wpmat2_v(3)


        INTEGER :: i, n

        n = SIZE(wp2pot_v, 1)  ! atomic = 1, molecular = 3

        DO i = 1, 3
            wpmat2_v(i) = wp2pot_v(MIN(i, n), ich-jch, jch)
        END DO


    END SUBROUTINE makewpmat2_v

!---------------------------------------------------------------------------

    SUBROUTINE makewdpmat(ich, jch, wdpmat)

        USE lrpots, ONLY:  wdpot

        INTEGER, INTENT(IN)      :: ich, jch
        COMPLEX(wp), INTENT(OUT) :: wdpmat(3)

        INTEGER :: i, n

        n = SIZE(wdpot, 1)  ! atomic = 1, molecular = 3)

        DO i = 1, 3
            wdpmat(i) = wdpot(MIN(i, n), ich - jch, jch)
        END DO

    END SUBROUTINE makewdpmat

!---------------------------------------------------------------------------

    SUBROUTINE makewdpmat_v(ich, jch, wdpmat_v)

    ! DDAC: Required in velocity gauge

        USE lrpots, ONLY: wdpot_v

        INTEGER, INTENT(IN) :: ich, jch
        COMPLEX(wp), INTENT(OUT) :: wdpmat_v(3)

        INTEGER :: i, n

        n = SIZE(wdpot_v, 1)  ! atomic = 1, molecular = 3)

        DO i = 1, 3
            wdpmat_v(i) = wdpot_v(MIN(i, n), ich - jch, jch)
        END DO


    END SUBROUTINE makewdpmat_v

END MODULE potentials
