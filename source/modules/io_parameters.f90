! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Sets parameters for the naming of output files.

MODULE io_parameters

    USE precisn

    IMPLICIT NONE

    PUBLIC

    CHARACTER(LEN=8), SAVE  :: intensity_image
    CHARACTER(LEN=26), SAVE :: version

CONTAINS

    SUBROUTINE init_io_parameters

        USE initial_conditions, ONLY: version_root, intensity
        USE calculation_parameters, ONLY: scaling_factor_for_i_image

        INTEGER           :: intensity_id
        CHARACTER(LEN=8)  :: intensity_image

        intensity_id = NINT(intensity(1)*scaling_factor_for_i_image)

        CALL get_image_of8(intensity_id, intensity_image)

        version = TRIM(ADJUSTL(version_root))//intensity_image

    END SUBROUTINE init_io_parameters

!---------------------------------------------------------------------------
! Right justified 6 digit image of integer
!---------------------------------------------------------------------------

    SUBROUTINE get_image_of8(value_of_int, image)

        INTEGER, INTENT(IN)           :: value_of_int
        CHARACTER(LEN=8), INTENT(OUT) :: image
        INTEGER                       :: value
        CHARACTER(LEN=1)              :: digit(0:9)
        INTEGER                       :: digit0, digit1, digit2
        INTEGER                       :: digit3, digit4, digit5
        INTEGER                       :: digit6, digit7

        DATA digit/'0', '1', '2', '3', '4', '5', '6', '7', '8', '9'/

        value = value_of_int

        IF (value > 99999999) THEN
            PRINT *, 'Image not in Range 0-99999999 '
        END IF

        IF (value < 0) THEN
            PRINT *, 'Image not in Range 0-99999999 '
        END IF

        digit7 = value/10000000
        value = value - digit7*10000000
        digit6 = value/1000000
        value = value - digit6*1000000
        digit5 = value/100000
        value = value - digit5*100000
        digit4 = value/10000
        value = value - digit4*10000
        digit3 = value/1000
        value = value - digit3*1000
        digit2 = value/100
        value = value - digit2*100
        digit1 = value/10
        value = value - digit1*10
        digit0 = value/1

        image = digit(digit7)//digit(digit6)//digit(digit5)//digit(digit4)// &
                digit(digit3)//digit(digit2)//digit(digit1)//digit(digit0)

    END SUBROUTINE get_image_of8


END MODULE io_parameters
