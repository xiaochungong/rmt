! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Controls the flow of information from the outer to inner region,
!> specifically the calculation of the boundary amplitudes for propagation of
!> the outer region (grid) wavefunction into the inner region basis
!> representation.
!> Note that a separate module,@ref inner_to_outer_interface, handles the
!> flow of information in the other direction.

MODULE outer_to_inner_interface

    USE precisn,    ONLY: wp
    USE rmt_assert, ONLY: assert
    USE readhd,     ONLY: LML_block_tot_nchan
    USE initial_conditions, ONLY: numsols => no_of_field_confs
    USE MPI

    IMPLICIT NONE

    COMPLEX(wp), ALLOCATABLE  :: fderivs(:, :)

    PUBLIC send_fderivs_outer_to_inner
    PUBLIC get_fderivs_and_project
    PUBLIC allocate_fderivs
    PUBLIC deallocate_fderivs

CONTAINS

    SUBROUTINE send_fderivs_outer_to_inner(fderivs)

        IMPLICIT NONE

        COMPLEX(wp), INTENT(IN) :: fderivs(1:LML_block_tot_nchan, 1:numsols)

        INTEGER :: reqd_id_of_inner_pe, tag, ierror

        ! Find out which inner region processor handles this propagation order
!       Reqd_ID_of_Inner_PE = Master_Inner_PE_ID_for_Order(k1)
        Reqd_ID_of_Inner_PE = 0 ! perhaps should be PE_ID_1st?

        ! Send data
        tag = 3
        CALL MPI_SEND(fderivs, LML_block_tot_nchan*numsols, MPI_DOUBLE_COMPLEX, &
                      reqd_id_of_inner_pe, tag, MPI_COMM_WORLD, ierror)

    END SUBROUTINE send_fderivs_outer_to_inner

!---------------------------------------------------------------------

    SUBROUTINE get_fderivs_and_project(k1, vecout)

        USE initial_conditions,        ONLY: timings_desired
        USE communications_parameters, ONLY: id_of_1st_pe_outer
        USE distribute_hd_blocks2,     ONLY: numrows
        USE mpi_communications,        ONLY: get_my_pe_id
        USE wall_clock,                ONLY: update_time_start_inner, &
                                             update_time_end_inner

        IMPLICIT NONE

        INTEGER, INTENT(IN)         :: k1
        COMPLEX(wp), INTENT(OUT)    :: vecout(numrows, numsols)

        INTEGER :: my_rank, reqd_id_of_inner_pe, tag, ierror
        INTEGER :: status(MPI_STATUS_SIZE)

        fderivs = (0.0_wp, 0.0_wp)

        ! Get my rank
        CALL get_my_pe_id(my_rank)

        ! Find out which inner region processor handles this propagation order
        reqd_id_of_inner_pe = 0! Master_Inner_PE_ID_for_Order(k1)

        ! Receive data from outer region:
        IF (my_rank == reqd_id_of_inner_pe) THEN

            ! Timers
            IF (timings_desired) THEN
                CALL update_time_start_inner(k1)
            END IF

            tag = 3
            CALL MPI_RECV(fderivs, LML_block_tot_nchan*numsols, MPI_DOUBLE_COMPLEX, &
                          id_of_1st_pe_outer, tag, MPI_COMM_WORLD, status, ierror)
            ! Timers
            IF (timings_desired) THEN
                CALL update_time_end_inner
            END IF

        END IF

        CALL bcast_fderivs_to_Lb_masters

        CALL project_fderivs_onto_surf_amps(vecout)

    END SUBROUTINE get_fderivs_and_project

!---------------------------------------------------------------------

    SUBROUTINE bcast_fderivs_to_Lb_masters

        USE mpi_communications, ONLY: mpi_comm_region

        IMPLICIT NONE

        INTEGER   :: ierr

        CALL MPI_BCAST(fderivs, LML_block_tot_nchan*numsols, MPI_DOUBLE_COMPLEX, 0, mpi_comm_region, ierr)

    END SUBROUTINE bcast_fderivs_to_Lb_masters

!---------------------------------------------------------------------

    SUBROUTINE project_fderivs_onto_surf_amps(vecout)

        USE distribute_hd_blocks2, ONLY: loc_surf_amps, numrows

        IMPLICIT NONE

        COMPLEX(wp), INTENT(OUT)   :: vecout(numrows, numsols)

        COMPLEX(wp), PARAMETER     :: zihalf = (0.0_wp, 0.5_wp)
        COMPLEX(wp), PARAMETER     :: zero = (0.0_wp, 0.0_wp)

        IF (numrows > 0) THEN
            IF (numsols == 1) THEN
                CALL ZGEMV('N', numrows, LML_block_tot_nchan, zihalf, loc_surf_amps, numrows, fderivs(:, 1), &
                           1, zero, vecout(:, 1), 1)
            ELSE
                CALL ZGEMM('N', 'N', numrows, numsols, LML_block_tot_nchan, zihalf, loc_surf_amps, &
                           numrows, fderivs, LML_block_tot_nchan, zero, vecout, numrows)
            END IF
        END IF

    END SUBROUTINE project_fderivs_onto_surf_amps

!---------------------------------------------------------------------

    SUBROUTINE allocate_fderivs

        IMPLICIT NONE

        INTEGER  :: err

        ALLOCATE (fderivs(LML_block_tot_nchan, numsols), stat=err)
        CALL assert(err .EQ. 0, 'allocation error with Fderivs')

    END SUBROUTINE allocate_fderivs

!---------------------------------------------------------------------

    SUBROUTINE deallocate_fderivs

        IMPLICIT NONE

        INTEGER  :: err

        DEALLOCATE (fderivs, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error with Fderivs')

    END SUBROUTINE deallocate_fderivs

END MODULE outer_to_inner_interface
