! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Legacy file - not currently used in RMT.

MODULE io_units

    IMPLICIT NONE

    PRIVATE
    PUBLIC fi, fo !, dsk, dskp1, dskp2
!   PUBLIC set_dsk, set_dskp1, set_dskp2

! Ang values:
!   PUBLIC jbufiv, jbuffv, jbufie, jbuffe
!   PUBLIC jbufiv1, jbuffv1, jbufiv2, jbuffv2

! Ham values:
!   PUBLIC jbuff1, jbuff2, jbuff4, jbuff5, itape1, itape3, jbufd
!   PUBLIC jbufim, jbuffm, jbufev

!   PUBLIC jibb, jfbb, jibc, jfbc, fl, ihunit, itm
!   PUBLIC jbufivd, jbuffvd, jbufiv2d, jbuffv2d

    INTEGER, PARAMETER      :: fi = 5      ! standard input
    INTEGER, PARAMETER      :: fo = 6      ! standard output
    INTEGER, SAVE           :: dsk = 0     ! scratch disk unit
    INTEGER, SAVE           :: dskp1 = 0   ! scratch disk unit
    INTEGER, SAVE           :: dskp2 = 0   ! scratch disk unit

!   INTEGER, SAVE           :: jbufiv = 20
!   INTEGER, SAVE           :: jbuffv = 21
!   INTEGER, SAVE           :: jbuff1 = 24
!   INTEGER, SAVE           :: jbuff2 = 25
!   INTEGER, SAVE           :: itape1 = 26
!   INTEGER, SAVE           :: jbuff4 = 27
!   INTEGER, SAVE           :: jbufiv1 = 24    ! -> jbuff1
!   INTEGER, SAVE           :: jbuffv1 = 25    ! -> jbuff2
!   INTEGER, SAVE           :: jbufiv2 = 26    ! -> itape1
!   INTEGER, SAVE           :: jbuffv2 = 27    ! -> jbuff4
!   INTEGER, SAVE           :: jbuff5 = 28
!   INTEGER, SAVE           :: jbufd = 29
!   INTEGER, SAVE           :: itape3 = 10

!   INTEGER, SAVE           :: jbufie = 22
!   INTEGER, SAVE           :: jbuffe = 23

!   INTEGER, SAVE           :: jbufim = 45
!   INTEGER, SAVE           :: jbuffm = 46

!   INTEGER, SAVE           :: jbufev = 47

! Dipole units used in stmmat:
!   INTEGER, SAVE           :: jbufivd = 41
!   INTEGER, SAVE           :: jbuffvd = 42
!   INTEGER, SAVE           :: jbufiv2d = 43
!   INTEGER, SAVE           :: jbuffv2d = 44

!   INTEGER, SAVE           :: jibb = 30    ! abbi
!   INTEGER, SAVE           :: jfbb = 31    ! abbr
!   INTEGER, SAVE           :: jibc = 32    ! abci
!   INTEGER, SAVE           :: jfbc = 33    ! abcr

!   INTEGER, SAVE           :: fl = 36      ! tgt dataset unit
!   INTEGER, SAVE           :: ihunit = 37  ! unit for Hamiltonian output
!   INTEGER, SAVE           :: itm = 38     ! unit for tarmom file

CONTAINS

    SUBROUTINE set_dsk(dsk_unit)

        INTEGER, INTENT(IN)   :: dsk_unit

        dsk = dsk_unit
        RETURN

    END SUBROUTINE set_dsk

!-----------------------------------------------------------------------

    SUBROUTINE set_dskp1(dsk_unit)

        INTEGER, INTENT(IN)   :: dsk_unit

        dskp1 = dsk_unit
        RETURN

    END SUBROUTINE set_dskp1

!-----------------------------------------------------------------------

    SUBROUTINE set_dskp2(dsk_unit)

        INTEGER, INTENT(IN)   :: dsk_unit

        dskp2 = dsk_unit
        RETURN

    END SUBROUTINE set_dskp2

END MODULE io_units
