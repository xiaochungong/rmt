! Copyright 2018 
!
! This file is part of RMT.
! 
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
! 
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
! 
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit 
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Routines for handling outer region Hamiltonian-wavefunction multiplication.
!> Including incrementing with the second derivative and Laplacian, 
!> applying the absorbing boundary, and calculating the ionizated population. 
!> Incrementing with the long-range potential matrices, is handled in @ref outer_hamiltonian 
!> and @ref outer_hamiltonian_atrlessthanb.

MODULE local_ham_matrix

    USE precisn,                ONLY: wp
    USE rmt_assert,             ONLY: assert
    USE calculation_parameters, ONLY: Lmax_At_bndry, &
                                      R_cutoff
    USE grid_parameters,        ONLY: x_last, &
                                      x_1st, &
                                      R_1st, &
                                      R_last
    USE initial_conditions,     ONLY: deltaR,&
                                      window_harmonics,&
                                      window_cutoff,&
                                      window_FWHM

    IMPLICIT NONE

    PUBLIC

    ! Absorbing pot
!   REAL(wp), PRIVATE,SAVE  ::  absorb    (x_1st:x_Last)
!   REAL(wp), PRIVATE,SAVE  ::  absorb_sq (x_1st:x_Last)
   
!   REAL(wp),SAVE           ::  RR        (x_1st:x_last)
!   REAL(wp),SAVE           ::  Rinverse  (x_1st:x_last)
!   REAL(wp),SAVE           ::  Rinverse2 (x_1st:x_last)
!   REAL(wp),SAVE           ::  one_over_delR
!   INTEGER,SAVE            ::  R_id_at   (x_1st:x_last)

    REAL(wp), PRIVATE, SAVE, ALLOCATABLE  ::  absorb(:)
    REAL(wp), PRIVATE, SAVE, ALLOCATABLE  ::  absorb_sq(:)
 
    REAL(wp), SAVE, ALLOCATABLE           ::  master_RR(:)
    REAL(wp), SAVE, ALLOCATABLE           ::  windowed_RR(:)
    REAL(wp), SAVE, ALLOCATABLE           ::  RR(:)
    Real(wp), SAVE                        ::  r_value_at_r1st
    REAL(wp), SAVE, ALLOCATABLE           ::  Rinverse(:)
    REAL(wp), SAVE, ALLOCATABLE           ::  Rinverse2(:)
    REAL(wp), SAVE                        ::  one_over_delR
    REAL(wp), SAVE, ALLOCATABLE           ::  Rweight(:)
    INTEGER, SAVE, ALLOCATABLE            ::  R_id_at(:)


CONTAINS
 
    SUBROUTINE init_local_ham_matrix_module(r_at_region_bndry, delta_r_value, my_r_start_id, &
                                            i_am_outer_master)

        USE calculation_parameters, ONLY: single_ioniz_bndry_in_au, &
                                          single_ioniz_bndry_in_grid_pts,&
                                          nfdm,&
                                          half_fd_order
        USE grid_parameters,        ONLY: channel_id_1st, &
                                          channel_id_last
        USE mpi_communications,     ONLY: get_my_pe_id
 
        REAL(wp), INTENT(IN)   :: r_at_region_bndry
        REAL(wp), INTENT(IN)   :: delta_r_value
        INTEGER, INTENT(IN)    :: my_r_start_id
        LOGICAL, INTENT(IN)    :: i_am_outer_master
        INTEGER                :: i, err
        INTEGER                :: my_pe_id, ch_id 
        INTEGER :: max_points_for_H_op

        max_points_for_H_op = 2 * nfdm - half_fd_order

        ! ALLOCATIONS
        ! Hugo: NOTE x_1st assumed to equal 1
        ALLOCATE (absorb(x_1st:x_last), absorb_sq(x_1st:x_last), &
                  master_RR(max_points_for_H_op), &
                  windowed_RR(x_1st:x_last),&
                  RR(x_1st:x_last), Rinverse(x_1st:x_last), &
                  Rinverse2(x_1st:x_last), R_id_at(x_1st:x_last), &
                  Rweight(x_1st:x_last*(channel_id_last - channel_id_1st + 1)), stat=err)
        CALL assert(err .EQ. 0, 'allocation error in init_local_ham_matrix')

        ! INITIALIZE CONSTANT ARRAYS
        define_r_id: DO i = x_1st, x_last
            R_id_at(i) = my_r_start_id - R_1st + 1 + i - x_1st
        END DO define_r_ID

        define_grid: DO i = x_1st, x_last
            IF (r_at_region_bndry .GT. 0) THEN
                RR(i) = r_at_region_bndry + deltaR*(R_id_at(i) - 1)  ! RR(x_1st) = r_at_region_bndry on 1st PE in outer region
            ELSE
                RR(i) = deltaR*R_id_at(i)
            END IF
        END DO define_grid

        r_value_at_r1st = RR(x_1st)

        DO i = 1, max_points_for_H_op
            master_RR(i) = r_value_at_r1st + deltaR * REAL(-nfdm+i-1,wp)
        END DO

        DO i = x_1st, x_last
            IF ((RR(i) > window_cutoff) .AND. (window_harmonics)) THEN
                windowed_RR(i) = RR(i) * exp(-(window_cutoff - RR(i))*(window_cutoff - RR(i))/(window_FWHM*window_FWHM))
            ELSE
                windowed_RR(i) = RR(i)
            END IF
        END DO


        DO i = x_1st, x_last
            Rinverse(i) = 1.0_wp/RR(i)
        END DO

        DO i = x_1st, x_last
            Rinverse2(i) = 1.0_wp/(RR(i)*RR(i))
        END DO

        CALL get_my_pe_id(my_pe_id)

        ! Change - the last grid point is 1 beyond the end point of the last grid element
        ! where the value is set to 0. Thus slightly amend the grid: all sectors should
        ! be a multiple of 4.

!       IF (i_am_outer_master) THEN
!           DO ch_id=channel_id_1st, channel_id_last
!               rweight(x_1st+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 28._wp/90._wp
!               DO i = x_1st+1, x_last-3, 4
!                   rweight(i+(ch_id-channel_id_1st)*(x_last-x_1st+1))   = 128._wp/90._wp
!                   rweight(i+1+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 48._wp/90._wp
!                   rweight(i+2+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 128._wp/90._wp
!                   rweight(i+3+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 56._wp/90._wp
!               END DO
!               IF (my_pe_id .EQ. pe_id_last) THEN
!                   rweight(x_last+(ch_id-channel_id_1st)*(x_last-x_1st+1))=28._wp/90._wp
!               END IF
!           END DO
!       ELSE
!           DO ch_id=channel_id_1st, channel_id_last
!               DO i = x_1st, x_last-3, 4
!                   rweight(i+(ch_id-channel_id_1st)*(x_last-x_1st+1))   = 128._wp/90._wp
!                   rweight(i+1+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 48._wp/90._wp
!                   rweight(i+2+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 128._wp/90._wp
!                   rweight(i+3+(ch_id-channel_id_1st)*(x_last-x_1st+1)) = 56._wp/90._wp
!               END DO
!               IF (my_pe_id .EQ. pe_id_last) THEN
!                   rweight(x_last+(ch_id-channel_id_1st)*(x_last-x_1st+1))=28._wp/90._wp
!               END IF
!           END DO
!       END IF

        DO ch_id = channel_id_1st, channel_id_last
            DO i = x_1st, x_last - 3, 4
                rweight(i + (ch_id - channel_id_1st)*(x_last - x_1st + 1)) = 56._wp/90._wp
                rweight(i + 1 + (ch_id - channel_id_1st)*(x_last - x_1st + 1)) = 128._wp/90._wp
                rweight(i + 2 + (ch_id - channel_id_1st)*(x_last - x_1st + 1)) = 48._wp/90._wp
                rweight(i + 3 + (ch_id - channel_id_1st)*(x_last - x_1st + 1)) = 128._wp/90._wp
            END DO
            IF (i_am_outer_master) THEN
                rweight(x_1st + (ch_id - channel_id_1st)*(x_last - x_1st + 1)) = 28._wp/90._wp
            END IF
        END DO

        one_over_delR = 1.0_wp/delta_r_value

        ! Find the value (in grid points) of the boundary beyond which we have single ionization
        single_ioniz_bndry_in_grid_pts = NINT(single_ioniz_bndry_in_au/deltaR)
        single_ioniz_bndry_in_grid_pts = MAX(R_1st, single_ioniz_bndry_in_grid_pts)
        single_ioniz_bndry_in_grid_pts = MIN(R_Last, single_ioniz_bndry_in_grid_pts)

        ! Initialize the Absorb array

        CALL init_absorb

        CALL test_derivatives_of_r(RR, delta_r_value)

    END SUBROUTINE init_local_ham_matrix_module

!-------------------------------------------------------------------------------------

    SUBROUTINE dealloc_local_ham_matrix

        INTEGER :: err

        DEALLOCATE (absorb, absorb_sq, &
                    RR, Rinverse, &
                    Rinverse2, stat=err)
        CALL assert(err .EQ. 0, 'deallocation error in init_local_ham_matrix')

    END SUBROUTINE dealloc_local_ham_matrix

!-------------------------------------------------------------------------------------

    SUBROUTINE test_derivatives_of_r(var, delta_r_value)
 
        REAL(wp), INTENT(IN) :: var(x_1st:x_last)
        REAL(wp), INTENT(IN) :: delta_r_value
        REAL(wp)             :: r, fd_spacing, inv_fd_spacing, inv_fd_spacing_sq
        COMPLEX(wp)          :: poly(x_1st:x_last)
        COMPLEX(wp)          :: exact_1st_deriv_poly(x_1st:x_last)
        COMPLEX(wp)          :: fd_1st_deriv_poly(x_1st:x_last)
        COMPLEX(wp)          :: fd_2nd_deriv_poly(x_1st:x_last)
        INTEGER              :: i
        COMPLEX(wp)          :: first_2_points(x_1st - 2:x_1st - 1)
        COMPLEX(wp)          :: last_2_points(x_last + 1:x_last + 2)
        COMPLEX(wp)          :: answer1(x_1st:x_last)
        REAL(wp)             :: max_error1, error1

        fd_spacing = delta_r_value
        inv_fd_spacing = 1.0_wp/fd_spacing
        inv_fd_spacing_sq = inv_fd_spacing**2

        ! TEST Get4_1st_Deriv

        DO i = x_1st, x_last
            r = var(i)
            poly(i) = EXP(-r)
            exact_1st_deriv_poly(i) = -poly(i)
        END DO

        ! Need to pass values at 2 points inside poly
        ! Values could be arbitrarily as test of values is only for x_1st+2:x_last-2
        DO i = x_1st - 2, x_1st - 1
            r = var(i + 2) - (2*delta_r_value)
            first_2_points(i) = EXP(-r)
        END DO

        DO i = x_last + 1, x_last + 2
            r = var(i - 2) + (2*delta_r_value)
            last_2_points(i) = EXP(-r)
        END DO

        CALL get4_1st_deriv(poly, fd_1st_deriv_poly, first_2_points, last_2_points, inv_fd_spacing)

        max_error1 = 0.0_wp

        DO i = x_1st + 2, x_last - 2
!           PRINT*,'i=',i,' poly =',poly(i), ' FD 1st deriv=', fd_1st_deriv_poly(i)
            answer1(i) = (poly(i) + fd_1st_deriv_poly(i))
            error1 = ABS(REAL(answer1(i)))
            IF (error1 > max_error1) THEN
                max_error1 = error1
            END IF
        END DO

!       PRINT*,'max error=',max_error1
        CALL assert(max_error1 .le. 0.001, 'FAILURE 1st Deriv in R!')

        ! TEST INCR4_with_2nd_Deriv

        DO i = x_1st, x_last
            r = var(i)
            poly(i) = EXP(-r)
            exact_1st_deriv_poly(i) = poly(i)
        END DO

        ! Need to pass values at 2 points inside poly
        ! Values could be arbitrarily as test of values is only for x_1st+2:x_last-2
        DO i = x_1st - 2, x_1st - 1
            r = var(i + 2) - (2*delta_r_value)
            first_2_points(i) = EXP(-r)
        END DO

        DO i = x_last + 1, x_last + 2
            r = var(i - 2) + (2*delta_r_value)
            last_2_points(i) = EXP(-r)
        END DO

        fd_2nd_deriv_poly = (0.0_wp, 0.0_wp)

        CALL incr4_with_2nd_deriv(poly, fd_2nd_deriv_poly, & 
                                  inv_fd_spacing_sq, first_2_points, last_2_points)

        max_error1 = 0.0_wp

        DO i = x_1st + 2, x_last - 2
            answer1(i) = (poly(i) - fd_2nd_deriv_poly(i))
            error1 = ABS(REAL(answer1(i)))
            IF (error1 > max_error1) THEN
                max_error1 = error1
            END IF
        END DO

        CALL assert(max_error1 .LE. 0.001, 'FAILURE 2nd Deriv in R!')

    END SUBROUTINE test_derivatives_of_r

!--------------------------------------------------------------------

    SUBROUTINE get4_1st_deriv(psi, deriv_psi, R_bndry_lo, R_bndry_hi, inv_fd_spacing)
 
        COMPLEX(wp), INTENT(IN)  :: psi(x_1st:x_last)
        COMPLEX(wp), INTENT(OUT) :: deriv_psi(x_1st:x_last)
        COMPLEX(wp), INTENT(IN)  :: R_bndry_lo(x_1st - 2:x_1st - 1)
        COMPLEX(wp), INTENT(IN)  :: R_bndry_hi(x_last + 1:x_last + 2)
        REAL(wp), INTENT(IN)     :: inv_fd_spacing
        INTEGER                  :: i
        REAL(wp)                 :: D1(-2:2)

        ! h_psi_b(1:2) are values at x_1st - 1, x_1st - 2

        !------------------------------------------------------------
        ! 3rd ORDER DIFFERENTIATION CONSTANTS (1ST DERIVATIVE)
        !------------------------------------------------------------
!       D1(2)  =  -1.0_wp / 12.0_wp
!       D1(1)  =   8.0_wp / 12.0_wp
!       D1(0)  =   0.0_wp
!       D1(-1) =  -8.0_wp / 12.0_wp
!       D1(-2) =   1.0_wp / 12.0_wp

        ! Write in as decimal as otherwise the computer might be dividing
        ! every time this subroutine is called - slow

        D1(2) = -0.08333333333333333333333333_wp
        D1(1) = 0.66666666666666666666666667_wp
        D1(0) = 0.0_wp
        D1(-1) = -0.66666666666666666666666667_wp
        D1(-2) = 0.08333333333333333333333333_wp

        ! Take care of the 1/delta r pre-factor in the derivative
        D1 = D1*inv_fd_spacing

        deriv_psi(x_last) = &
            D1(-2)*psi(x_last - 2) + &
            D1(-1)*psi(x_last - 1) + &
            D1(1)*R_bndry_hi(x_last + 1) + &
            D1(2)*R_bndry_hi(x_last + 2)

        deriv_psi(x_last - 1) = &
            D1(-2)*(psi(x_last - 3) - R_bndry_hi(x_last + 1)) + &
            D1(-1)*(psi(x_last - 2) - psi(x_last))

        DO i = x_1st + 2, x_last - 2

            deriv_psi(i) = &
                D1(1)*(psi(i + 1) - psi(i - 1)) + &
                D1(2)*(psi(i + 2) - psi(i - 2))

        END DO

        deriv_psi(x_1st + 1) = &
            D1(1)*(psi(x_1st + 2) - psi(x_1st)) + &
            D1(2)*(psi(x_1st + 3) - R_bndry_lo(x_1st - 1))

        deriv_psi(x_1st) = &
            D1(1)*(psi(x_1st + 1) - R_bndry_lo(x_1st - 1)) + &
            D1(2)*(psi(x_1st + 2) - R_bndry_lo(x_1st - 2))

    END SUBROUTINE get4_1st_deriv

!--------------------------------------------------------------------

    SUBROUTINE get4_1st_deriv_at_b(psi_near_b, deriv_psi_at_b, &
                                   r_bndry_lo, inv_fd_spacing)
 
        COMPLEX(wp), INTENT(IN)  :: psi_near_b(x_1st + 1:x_1st + 2)
        COMPLEX(wp), INTENT(OUT) :: deriv_psi_at_b
        COMPLEX(wp), INTENT(IN)  :: r_bndry_lo(x_1st - 2:x_1st - 1)
        REAL(wp), INTENT(IN)     :: inv_fd_spacing
        REAL(wp)                 :: D1(-2:2)

        !------------------------------------------------------------
        ! 3rd ORDER DIFFERENTIATION CONSTANTS (1ST DERIVATIVE)
        !------------------------------------------------------------
!       D1(2)  =  -1.0_wp / 12.0_wp
!       D1(1)  =   8.0_wp / 12.0_wp
!       D1(0)  =   0.0_wp
!       D1(-1) =  -8.0_wp / 12.0_wp
!       D1(-2) =   1.0_wp / 12.0_wp

        ! Write in as decimal as otherwise the computer might be dividing
        ! every time this subroutine is called - slow

        D1(2) = -0.08333333333333333333333333_wp
        D1(1) = 0.66666666666666666666666667_wp
        D1(0) = 0.0_wp
        D1(-1) = -0.66666666666666666666666667_wp
        D1(-2) = 0.08333333333333333333333333_wp

        ! Take care of the 1/delta r pre-factor in the derivative
        D1 = D1*inv_fd_spacing

        deriv_psi_at_b = &
            D1(1)*(psi_near_b(x_1st + 1) - r_bndry_lo(x_1st - 1)) + &
            D1(2)*(psi_near_b(x_1st + 2) - r_bndry_lo(x_1st - 2))

    END SUBROUTINE get4_1st_deriv_at_b

!--------------------------------------------------------------------

    SUBROUTINE get4_1st_deriv_b(psi, deriv_psi, h_psi_b, &
                                inv_fd_spacing, num_grid_points)
 
        INTEGER, INTENT(IN)      :: num_grid_points
        COMPLEX(wp), INTENT(IN)  :: psi(1:num_grid_points)
        COMPLEX(wp), INTENT(OUT) :: deriv_psi(1:num_grid_points)
        COMPLEX(wp), INTENT(IN)  :: h_psi_b(1:4)
        REAL(wp), INTENT(IN)     :: inv_fd_spacing
        INTEGER                  :: i
        REAL(wp)                 :: D1(-2:2)

        ! h_psi_b(1:4) are values at first_pt - 2, first_pt - 1, last_pt + 1, last_pt + 2
 
        !-------------------------------------------------------------
        ! 3rd ORDER DIFFERENTIATION CONSTANTS (1ST DERIVATIVE)
        !-------------------------------------------------------------
!       D1(2)  =  -1.0_wp / 12.0_wp
!       D1(1)  =   8.0_wp / 12.0_wp
!       D1(0)  =   0.0_wp
!       D1(-1) =  -8.0_wp / 12.0_wp
!       D1(-2) =   1.0_wp / 12.0_wp

        ! Write in as decimal as otherwise the computer might be dividing
        ! every time this subroutine is called - slow

        D1(2) = -0.08333333333333333333333333_wp
        D1(1) = 0.66666666666666666666666667_wp
        D1(0) = 0.0_wp
        D1(-1) = -0.66666666666666666666666667_wp
        D1(-2) = 0.08333333333333333333333333_wp

        ! Take care of the 1/delta r pre-factor in the derivative
        D1 = D1*inv_fd_spacing

        IF (num_grid_points .EQ. 1) THEN

            deriv_psi(1) = D1(-2)*(h_psi_b(1) - h_psi_b(4)) + &
                           D1(-1)*(h_psi_b(2) - h_psi_b(3))

        ELSE IF (num_grid_points .EQ. 2) THEN

            deriv_psi(2) = D1(-2)*(h_psi_b(2) - h_psi_b(4)) + &
                           D1(-1)*(psi(1) - h_psi_b(3))
            deriv_psi(1) = D1(-2)*(h_psi_b(1) - h_psi_b(3)) + &
                           D1(-1)*(h_psi_b(2) - psi(2))

        ELSE IF (num_grid_points .EQ. 3) THEN

            deriv_psi(3) = D1(-2)*(psi(1) - h_psi_b(4)) + &
                           D1(-1)*(psi(2) - h_psi_b(3))
            deriv_psi(2) = D1(-2)*(h_psi_b(2) - h_psi_b(3)) + &
                           D1(-1)*(psi(1) - psi(3))
            deriv_psi(1) = D1(-2)*(h_psi_b(1) - psi(3)) + &
                           D1(-1)*(h_psi_b(2) - psi(2))

        ELSE IF (num_grid_points .EQ. 4) THEN

            deriv_psi(4) = D1(-2)*(psi(2) - h_psi_b(4)) + &
                           D1(-1)*(psi(3) - h_psi_b(3))
            deriv_psi(3) = D1(-2)*(psi(1) - h_psi_b(3)) + &
                           D1(-1)*(psi(2) - psi(4))
            deriv_psi(2) = D1(-2)*(h_psi_b(2) - psi(4)) + &
                           D1(-1)*(psi(1) - psi(3))
            deriv_psi(1) = D1(-2)*(h_psi_b(1) - psi(3)) + &
                           D1(-1)*(h_psi_b(2) - psi(2))

        ELSE

            deriv_psi(num_grid_points) = &
                D1(-2)*(psi(num_grid_points - 2) - h_psi_b(4)) + &
                D1(-1)*(psi(num_grid_points - 1) - h_psi_b(3))

            deriv_psi(num_grid_points - 1) = &
                D1(-2)*(psi(num_grid_points - 3) - h_psi_b(3)) + &
                D1(-1)*(psi(num_grid_points - 2) - psi(num_grid_points))

            DO i = 3, num_grid_points - 2

                deriv_psi(i) = &
                    D1(1)*(psi(i + 1) - psi(i - 1)) + &
                    D1(2)*(psi(i + 2) - psi(i - 2))
            END DO

            deriv_psi(2) = &
                D1(1)*(psi(3) - psi(1)) + &
                D1(2)*(psi(4) - h_psi_b(2))

            deriv_psi(1) = &
                D1(1)*(psi(2) - h_psi_b(2)) + &
                D1(2)*(psi(3) - h_psi_b(1))

        END IF

    END SUBROUTINE get4_1st_deriv_b

!---------------------------------------------------------------------

    SUBROUTINE incr_with_laplacian(my_psi, RESULT, &
                                   L, second_deriv_coeff, atomic_ham_factor, &
                                   one_over_r_sq, R_bndry_lo, R_bndry_hi)

        COMPLEX(wp), INTENT(IN)    :: my_psi(x_1st:x_last)
        COMPLEX(wp), INTENT(INOUT) :: RESULT(x_1st:x_last)

        REAL(wp), INTENT(IN)       :: second_deriv_coeff
        REAL(wp), INTENT(IN)       :: atomic_ham_factor
        REAL(wp), INTENT(IN)       :: one_over_r_sq(x_1st:x_last)
        INTEGER, INTENT(IN)        :: L
        COMPLEX(wp), INTENT(IN)    :: R_bndry_lo(x_1st - 2:x_1st - 1)
        COMPLEX(wp), INTENT(IN)    :: R_bndry_hi(x_last + 1:x_last + 2)

        ! Increment Result with the Centripetal terms from the Kinetic Ham.

        CALL apply_centripetal(my_psi, RESULT, L,atomic_ham_factor,one_over_r_sq, RR(x_1st), x_last - x_1st + 1)

        CALL incr4_with_2nd_deriv(my_psi, RESULT, second_deriv_coeff, R_bndry_lo, R_bndry_hi)

    END SUBROUTINE incr_with_laplacian

    SUBROUTINE apply_centripetal (psi, h_psi, L, atomic_ham_factor, one_over_r_sq, r_val_at_bndry, number_grid_points)

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: h_psi(number_grid_points)
        REAL(wp), INTENT(IN)       :: atomic_ham_factor
        REAL(wp), INTENT(IN)       :: r_val_at_bndry
        REAL(wp), INTENT(IN)       :: one_over_r_sq(number_grid_points)
        INTEGER, INTENT(IN)        :: L
        REAL(wp)                   :: centripetal(number_grid_points)
        INTEGER                    :: i, limit
        REAL(wp)                   :: C1, CL, delta_centrip_at_limit

        centripetal = REAL(L*(L + 1), wp)*atomic_ham_factor*one_over_r_sq

        IF ((r_val_at_bndry .LT. R_cutoff) .AND. (L .GT. Lmax_At_bndry)) THEN

            limit = NINT((R_cutoff - r_val_at_bndry)/deltaR)
            CL = REAL(Lmax_At_bndry*(Lmax_At_bndry + 1), wp)

            delta_centrip_at_limit = centripetal(limit) - CL*atomic_ham_factor*one_over_r_sq(limit)

            DO i = 1, limit - 1
                C1 = CL*atomic_ham_factor*one_over_r_sq(i)
                centripetal(i) = C1 + delta_centrip_at_limit
            END DO

        END IF

        DO i = 1, number_grid_points
            h_psi(i) = h_psi(i) - psi(i)*centripetal(i)
        END DO

    
    END SUBROUTINE apply_centripetal
!---------------------------------------------------------------------

    SUBROUTINE incr_with_laplacian_b(my_psi, RESULT, &
                                     L, second_deriv_coeff, atomic_ham_factor, &
                                     one_over_r_sq, h_psi_b, num_grid_points, &
                                     r_value_inner_bndry)

        INTEGER, INTENT(IN)        :: num_grid_points
        COMPLEX(wp), INTENT(IN)    :: my_psi(1:num_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: RESULT(1:num_grid_points)

        REAL(wp), INTENT(IN)       :: second_deriv_coeff
        REAL(wp), INTENT(IN)       :: atomic_ham_factor
        REAL(wp), INTENT(IN)       :: one_over_r_sq(1:num_grid_points)
        INTEGER, INTENT(IN)        :: L
        REAL(wp), INTENT(IN)       :: r_value_inner_bndry
        COMPLEX(wp), INTENT(IN)    :: h_psi_b(1:4)

        ! Increment Result with the Centripetal terms from the Kinetic Ham.

        CALL apply_centripetal(my_psi, RESULT, L,atomic_ham_factor,one_over_r_sq, r_value_inner_bndry, num_grid_points)

        CALL incr4_with_2nd_deriv_b(my_psi, RESULT, second_deriv_coeff, h_psi_b, num_grid_points)

    END SUBROUTINE incr_with_laplacian_b

!--------------------------------------------------------------------------------------

    SUBROUTINE incr4_with_2nd_deriv(psi, deriv_psi, second_deriv_coeff, R_bndry_lo, R_bndry_hi)

        COMPLEX(wp), INTENT(IN)    :: psi(x_1st:x_last)
        COMPLEX(wp), INTENT(INOUT) :: deriv_psi(x_1st:x_last)
        COMPLEX(wp), INTENT(IN)    :: R_bndry_lo(x_1st - 2:x_1st - 1)
        COMPLEX(wp), INTENT(IN)    :: R_bndry_hi(x_last + 1:x_last + 2)
        REAL(wp), INTENT(IN)       :: second_deriv_coeff
        INTEGER                    :: i
        REAL(wp)                   :: D2(-2:2)

        ! 3rd ORDER DIFFERENTIATION CONSTANTS (2ND DERIVATIVE)

!       D2(2)  =  -1.0_wp  / 12.0_wp
!       D2(1)  =   16.0_wp / 12.0_wp
!       D2(0)  =  -30.0_wp / 12.0_wp
!       D2(-1) =   16.0_wp / 12.0_wp
!       D2(-2) =  -1.0_wp  / 12.0_wp

        D2(2) = -0.083333333333333333333333333_wp
        D2(1) = 1.333333333333333333333333333_wp
        D2(0) = -2.500000000000000000000000000_wp
        D2(-1) = 1.333333333333333333333333333_wp
        D2(-2) = -0.083333333333333333333333333_wp

        DO i = -2, 2
            D2(i) = D2(i)*second_deriv_coeff
        END DO

        deriv_psi(x_last) = deriv_psi(x_last) + &
                            D2(-2)*psi(x_last - 2) + &
                            D2(-1)*psi(x_last - 1) + &
                            D2(0)*psi(x_last) + &
                            D2(1)*R_bndry_hi(x_last + 1) + &
                            D2(2)*R_bndry_hi(x_last + 2)

        deriv_psi(x_last - 1) = deriv_psi(x_last - 1) + &
                                D2(-2)*psi(x_last - 3) + &
                                D2(-1)*psi(x_last - 2) + &
                                D2(0)*psi(x_last - 1) + &
                                D2(1)*psi(x_last) + &
                                D2(2)*R_bndry_hi(x_last + 1)

        DO i = x_1st + 2, x_last - 2

            deriv_psi(i) = deriv_psi(i) + &
                           D2(-2)*psi(i - 2) + &
                           D2(-1)*psi(i - 1) + &
                           D2(0)*psi(i) + &
                           D2(1)*psi(i + 1) + &
                           D2(2)*psi(i + 2)
        END DO

        deriv_psi(x_1st + 1) = deriv_psi(x_1st + 1) + &
                               D2(-2)*R_bndry_lo(x_1st - 1) + &
                               D2(-1)*psi(x_1st) + &
                               D2(0)*psi(x_1st + 1) + &
                               D2(1)*psi(x_1st + 2) + &
                               D2(2)*psi(x_1st + 3)

        deriv_psi(x_1st) = deriv_psi(x_1st) + &
                           D2(-2)*R_bndry_lo(x_1st - 2) + &
                           D2(-1)*R_bndry_lo(x_1st - 1) + &
                           D2(0)*psi(x_1st) + &
                           D2(1)*psi(x_1st + 1) + &
                           D2(2)*psi(x_1st + 2)

    END SUBROUTINE incr4_with_2nd_deriv

!--------------------------------------------------------------------

    SUBROUTINE incr4_with_2nd_deriv_b(psi, deriv_psi, second_deriv_coeff, h_psi_b, &
                                      num_grid_points)

        INTEGER, INTENT(IN)        :: num_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(1:num_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: deriv_psi(1:num_grid_points)
        COMPLEX(wp), INTENT(IN)    :: h_psi_b(1:4)
        REAL(wp), INTENT(IN)       :: second_deriv_coeff
        INTEGER                    :: i
        REAL(wp)                   :: D2(-2:2)

        ! h_psi_b(1:4) are values at first_pt - 2, first_pt - 1, last_pt + 1, last_pt + 2

        ! 3rd ORDER DIFFERENTIATION CONSTANTS (2ND DERIVATIVE)

!       D2(2)  =  -1.0_wp  / 12.0_wp
!       D2(1)  =   16.0_wp / 12.0_wp
!       D2(0)  =  -30.0_wp / 12.0_wp
!       D2(-1) =   16.0_wp / 12.0_wp
!       D2(-2) =  -1.0_wp  / 12.0_wp

        D2(2) = -0.083333333333333333333333333_wp
        D2(1) = 1.333333333333333333333333333_wp
        D2(0) = -2.500000000000000000000000000_wp
        D2(-1) = 1.333333333333333333333333333_wp
        D2(-2) = -0.083333333333333333333333333_wp

        DO i = -2, 2
            D2(i) = D2(i)*second_deriv_coeff
        END DO

        IF (num_grid_points .EQ. 1) THEN

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*h_psi_b(3) + &
                           D2(2)*h_psi_b(4)

        ELSE IF (num_grid_points .EQ. 2) THEN

            deriv_psi(2) = deriv_psi(2) + &
                           D2(-2)*h_psi_b(2) + &
                           D2(-1)*psi(1) + &
                           D2(0)*psi(2) + &
                           D2(1)*h_psi_b(3) + &
                           D2(2)*h_psi_b(4)

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*psi(2) + &
                           D2(2)*h_psi_b(3)

        ELSE IF (num_grid_points .EQ. 3) THEN

            deriv_psi(3) = deriv_psi(3) + &
                           D2(-2)*psi(1) + &
                           D2(-1)*psi(2) + &
                           D2(0)*psi(3) + &
                           D2(1)*h_psi_b(3) + &
                           D2(2)*h_psi_b(4)

            deriv_psi(2) = deriv_psi(2) + &
                           D2(-2)*h_psi_b(2) + &
                           D2(-1)*psi(1) + &
                           D2(0)*psi(2) + &
                           D2(1)*psi(3) + &
                           D2(2)*h_psi_b(3)

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*psi(2) + &
                           D2(2)*psi(3)

        ELSE IF (num_grid_points .EQ. 4) THEN

            deriv_psi(4) = deriv_psi(4) + &
                           D2(-2)*psi(2) + &
                           D2(-1)*psi(3) + &
                           D2(0)*psi(4) + &
                           D2(1)*h_psi_b(3) + &
                           D2(2)*h_psi_b(4)

            deriv_psi(3) = deriv_psi(3) + &
                           D2(-2)*psi(1) + &
                           D2(-1)*psi(2) + &
                           D2(0)*psi(3) + &
                           D2(1)*psi(4) + &
                           D2(2)*h_psi_b(3)

            deriv_psi(2) = deriv_psi(2) + &
                           D2(-2)*h_psi_b(2) + &
                           D2(-1)*psi(1) + &
                           D2(0)*psi(2) + &
                           D2(1)*psi(3) + &
                           D2(2)*psi(4)

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*psi(2) + &
                           D2(2)*psi(3)

        ELSE

            deriv_psi(num_grid_points) = deriv_psi(num_grid_points) + &
                                         D2(-2)*psi(num_grid_points - 2) + &
                                         D2(-1)*psi(num_grid_points - 1) + &
                                         D2(0)*psi(num_grid_points) + &
                                         D2(1)*h_psi_b(3) + &
                                         D2(2)*h_psi_b(4)

            deriv_psi(num_grid_points - 1) = deriv_psi(num_grid_points - 1) + &
                                             D2(-2)*psi(num_grid_points - 3) + &
                                             D2(-1)*psi(num_grid_points - 2) + &
                                             D2(0)*psi(num_grid_points - 1) + &
                                             D2(1)*psi(num_grid_points) + &
                                             D2(2)*h_psi_b(3)

            DO i = 3, num_grid_points - 2

                deriv_psi(i) = deriv_psi(i) + &
                               D2(-2)*psi(i - 2) + &
                               D2(-1)*psi(i - 1) + &
                               D2(0)*psi(i) + &
                               D2(1)*psi(i + 1) + &
                               D2(2)*psi(i + 2)
            END DO

            deriv_psi(2) = deriv_psi(2) + &
                           D2(-2)*h_psi_b(2) + &
                           D2(-1)*psi(1) + &
                           D2(0)*psi(2) + &
                           D2(1)*psi(3) + &
                           D2(2)*psi(4)

            deriv_psi(1) = deriv_psi(1) + &
                           D2(-2)*h_psi_b(1) + &
                           D2(-1)*h_psi_b(2) + &
                           D2(0)*psi(1) + &
                           D2(1)*psi(2) + &
                           D2(2)*psi(3)

        END IF

    END SUBROUTINE incr4_with_2nd_deriv_b

!---------------------------------------------------------------------
! Akin to absorbing bc.  Split Psi into 2 parts.  Throw one away.
!---------------------------------------------------------------------

    SUBROUTINE local_split(del_R, psi, discarded_population, &
                           discarded_population_si, single_ioniz_bndry)

        REAL(wp), INTENT(IN)       :: del_R
        COMPLEX(wp), INTENT(INOUT) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)      :: discarded_population
        REAL(wp), INTENT(OUT)      :: discarded_population_si
        INTEGER, INTENT(IN)        :: single_ioniz_bndry

        INTEGER  :: i
        REAL(wp) :: sum_discarded_pop, sum_discarded_pop2, mod_1_minus_A


        ! Keep track of discarded population:
        sum_discarded_pop = 0.0_wp
        sum_discarded_pop2 = 0.0_wp
!       mod_1_minus_A = 1.0_ep - (Absorb)**2

        DO i = x_1st, x_last

            mod_1_minus_A = 1.0_wp - Absorb_sq(i)
            sum_discarded_pop = sum_discarded_pop + &
                                mod_1_minus_A*REAL(CONJG(psi(i))*psi(i), wp)

            ! Keep track of discarded population in the single ionization region:
            IF (R_id_at(i) > single_ioniz_bndry) THEN
                sum_discarded_pop2 = sum_discarded_pop2 + &
                                     mod_1_minus_A*REAL(CONJG(psi(i))*psi(i), wp)
            END IF

        END DO

        discarded_population = sum_discarded_pop*del_R
        discarded_population_si = sum_discarded_pop2*del_R

        ! Do absorption on psi:
        DO i = x_1st, x_last
            psi(i) = Absorb(i)*psi(i)
        END DO

    END SUBROUTINE local_split

!---------------------------------------------------------------------
!Absorption pot. (multiply this by the wave function)
!---------------------------------------------------------------------

    SUBROUTINE init_absorb

        USE initial_conditions, ONLY: start_factor, &
                                      sigma_factor

        INTEGER     :: i

        REAL(wp)    :: halfbox, sigma
        INTEGER     :: R_absorb_start


        absorb = 1.0_wp
        absorb_sq = 1.0_wp

        R_absorb_start = NINT(REAL(R_last, wp)*start_factor)

        halfbox = (R_last/2)*deltaR

        sigma = halfbox*sigma_factor

        DO i = x_1st, x_last
            IF ((R_id_at(i) - R_absorb_start) > 0) THEN
                absorb(i) = EXP(-(REAL(R_id_at(i) - R_absorb_start, wp)*deltaR/sigma)**2)
                absorb_sq(i) = absorb(i)**2
            END IF
        END DO

    END SUBROUTINE init_absorb

!-----------------------------------------------------------------------

    SUBROUTINE get_local_pop_outer_A(del_R, psi, population)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)   :: population

        INTEGER                 :: i1
        REAL(wp)                :: sum_population

        ! Total outer region

        ! Trapezoidal rule (with weighting of 1/2 for 1st grid point):

        sum_population = 0.0_wp
        DO i1 = x_1st + 1, x_last
            sum_population = sum_population + REAL(CONJG(psi(i1))*psi(i1), wp)
        END DO

        IF (R_id_at(x_1st) .EQ. 1) THEN
            sum_population = sum_population + 0.50_wp*REAL(CONJG(psi(x_1st))*psi(x_1st), wp)
        ELSE
            sum_population = sum_population + REAL(CONJG(psi(x_1st))*psi(x_1st), wp)
        END IF

        population = sum_population*del_R

    END SUBROUTINE get_local_pop_outer_A

!-----------------------------------------------------------------------

    SUBROUTINE get_local_pop_outer(del_R, psi, population)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)   :: population

        INTEGER                 :: i1
        REAL(wp)                :: sum_population

        ! Total outer region

        sum_population = 0.0_wp

        ! Hugo: with rweights defined properly, this should not be a problem

        DO i1 = x_1st, x_last
            sum_population = sum_population + REAL(CONJG(psi(i1))*psi(i1), wp) &
                             *rweight(i1)
        END DO

        ! Simpson's rule:
        ! Weights applied to grid point 1,2,3,4,5,6,7,....R_Last are 1,4,2,4,2,4,2,...1

        ! Hugo: this all commented out
!       IF (MOD(R_id_at(x_1st),2) .EQ. 0) THEN ! x_1st is an even grid point
!           DO i1 = x_1st+1,x_last-1,2
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(i1))*psi(i1), wp)
!           END DO
!           DO i1 = x_1st,x_last-1,2 ! Include x_1st in this loop (x_1st is even so cannot equal 1)
!               sum_population = sum_population + 4.0_wp*REAL (CONJG (psi(i1))*psi(i1), wp)
!           END DO
            ! Add on contribution from x_last:
!           IF (R_id_at(x_last) .EQ. (R_Last-R_1st+1)) THEN ! x_last grid point = R_Last
!               sum_population = sum_population + REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           ELSE IF (MOD(R_id_at(x_last),2) .EQ. 0)  THEN ! x_last is an even grid point
!               sum_population = sum_population + 4.0_wp*REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           ELSE  ! x_last is an odd grid point
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           END IF
!       ELSE  ! x_1st is an odd grid point
!           DO i1 = x_1st+1,x_last-1,2
!               sum_population = sum_population + 4.0_wp*REAL (CONJG (psi(i1))*psi(i1), wp)
!           END DO
!           DO i1 = x_1st+2,x_last-1,2
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(i1))*psi(i1), wp)
!           END DO
            ! Add on contribution from x_1st:
!           IF (R_id_at(x_1st) .EQ. 1) THEN ! first grid point on the boundary with the inner region
!               sum_population = sum_population + REAL (CONJG (psi(x_1st))*psi(x_1st), wp)
!           ELSE
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(x_1st))*psi(x_1st), wp)
!           END IF
            ! Add on contribution from x_last:
!           IF (R_id_at(x_last) .EQ. (R_Last-R_1st+1)) THEN ! x_last grid point = R_Last
!               sum_population = sum_population + REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           ELSE IF (MOD(R_id_at(x_last),2) .EQ. 0)  THEN ! x_last is an even grid point
!               sum_population = sum_population + 4.0_wp*REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           ELSE  ! x_last is an odd grid point
!               sum_population = sum_population + 2.0_wp*REAL (CONJG (psi(x_last))*psi(x_last), wp)
!           END IF
!       END IF

        population = sum_population

!       population = population*(del_R/3.0_wp)
        population = sum_population*del_R

    END SUBROUTINE get_local_pop_outer

!-----------------------------------------------------------------------

    SUBROUTINE get_local_pop_outer_si(del_R, psi, population, single_ioniz_bndry)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)   :: population
        INTEGER, INTENT(IN)     :: single_ioniz_bndry

        INTEGER                 :: i1
        REAL(wp)                :: sum_population

        ! Outer region beyond certain distance - SI (Single Ionization)
        ! Using trapezoidal rule

        sum_population = 0.0_wp
        DO i1 = x_1st, x_last
            IF (R_id_at(i1) > single_ioniz_bndry) THEN
                sum_population = sum_population + REAL(CONJG(psi(i1))*psi(i1), wp)
            END IF
        END DO

        population = sum_population*del_R

    END SUBROUTINE get_local_pop_outer_si

!-----------------------------------------------------------------------

    SUBROUTINE get_local_pop_outer_ryd(del_R, psi, population, single_ioniz_bndry)

        REAL(wp), INTENT(IN)    :: del_R
        COMPLEX(wp), INTENT(IN) :: psi(x_1st:x_last)
        REAL(wp), INTENT(OUT)   :: population
        INTEGER, INTENT(IN)     :: single_ioniz_bndry

        INTEGER                 :: i1
        REAL(wp)                :: sum_population

        ! Outer region beyond certain distance - SI (Single Ionization)
        ! Using trapezoidal rule

        sum_population = 0.0_wp
        DO i1 = x_1st, x_last
            IF (R_id_at(i1) .LE. single_ioniz_bndry) THEN
                sum_population = sum_population + REAL(CONJG(psi(i1))*psi(i1), wp)
            END IF
        END DO

        population = sum_population*del_R

    END SUBROUTINE get_local_pop_outer_ryd

END MODULE local_ham_matrix
