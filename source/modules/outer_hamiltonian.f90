! Copyright 2018
!
! This file is part of RMT.
!
!     RMT is free software: you can redistribute it and/or modify
!     it under the terms of the GNU General Public License as published by
!     the Free Software Foundation, either version 3 of the License, or
!     (at your option) any later version.
!
!     RMT is distributed in the hope that it will be useful,
!     but WITHOUT ANY WARRANTY; without even the implied warranty of
!     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!     GNU General Public License for more details.
!
!     You should have received a copy of the GNU General Public License
!     along with RMT (in rmt/COPYING). Alternatively, you can also visit
!     <https://www.gnu.org/licenses/>.
!

!> @ingroup source
!> @brief Manages operations of the outer region Hamiltonian (i.e. performing
!> the Hamiltonian-wavefunction multiplication, incrementing with the long range
!> potential matrices).

!> Original version for arbitrarily polarised fields by DDAC
!> Velocity gauge not yet implemented!

MODULE outer_hamiltonian

    USE rmt_assert,             ONLY: assert
    USE precisn,                ONLY: wp
    USE global_data,            ONLY: zero
    USE grid_parameters,        ONLY: channel_id_1st, &
                                      channel_id_last, &
                                      x_1st, &
                                      x_last
    USE coupling_rules,         ONLY: target_coupled,&
                                      projectile_lcoupled,&
                                      projectile_vcoupled,&
                                      field_contraction
    USE initial_conditions,     ONLY: numsols => no_of_field_confs
    USE local_ham_matrix,       ONLY: one_over_delR, &
                                      Rinverse, &
                                      Rinverse2, &
                                      RR, &
                                      get4_1st_deriv, &
                                      get4_1st_deriv_at_b
    USE lrpots,                 ONLY: liuu, &
                                      miuu, &
                                      lpuu, &
                                      ltuu, &
                                      lmuu, &
                                      etuu, &
                                      lsuu, &
                                      icuu, &
                                      ksq
    USE potentials,             ONLY: makewemat, &
                                      makewpmat, &
                                      makewpmat2, &
                                      makewdpmat, &
                                      makewdpmat_v
    USE readhd,                 ONLY: lmaxp1, &
                                      lstartm1, &
                                      no_of_LML_blocks
    USE mpi_communications,     ONLY: get_my_group_pe_id

    IMPLICIT NONE

    INTEGER, ALLOCATABLE, SAVE     :: wp_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: we_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: wd_neighbour_count(:)
    INTEGER, ALLOCATABLE, SAVE     :: wp_neighbour_store(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: we_neighbour_store(:,:)
    INTEGER, ALLOCATABLE, SAVE     :: wd_neighbour_store(:,:)

CONTAINS

    SUBROUTINE get_missing_2nd_deriv_bndries(psi_outer, R_bndry_hi)

        USE mpi_communications, ONLY: get_my_group_pe_id
        USE communications_parameters, ONLY: pe_id_last_outer
        COMPLEX(wp), INTENT(IN)      :: psi_outer(x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(INOUT)   :: R_bndry_hi(x_last + 1:x_last + 2, channel_id_1st:channel_id_last, 1:numsols)

        REAL(wp)   :: outer_boundary_ratio
        INTEGER    :: my_group_pe_id

        ! If I am the last processor in the outer region
        ! set R_bndry_hi(x_last+1) = zero
        ! and R_bndry_hi(x_last+2) = -1.0_wp * psi_outer(x_last)

        ! At the outer bndry, psi is odd for all L's:
        outer_boundary_ratio = -1.0_wp
        CALL get_my_group_pe_id(my_group_pe_id)
        IF (my_group_pe_id .EQ. pe_id_last_outer) THEN
            R_bndry_hi(x_last + 1, :, :) = zero
            R_bndry_hi(x_last + 2, :, :) = outer_boundary_ratio*psi_outer(x_last, :, :)
        END IF

    END SUBROUTINE get_missing_2nd_deriv_bndries

!---------------------------------------------------------------------

    SUBROUTINE get_missing_1st_deriv_bndries(psi_outer, R_bndry_hi)

        USE mpi_communications, ONLY: get_my_group_pe_id
        USE communications_parameters, ONLY: pe_id_last_outer

        COMPLEX(wp), INTENT(IN)      :: psi_outer(x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(INOUT)   :: R_bndry_hi(x_last + 1:x_last + 2, channel_id_1st:channel_id_last, 1:numsols)

        REAL(wp)   :: outer_boundary_ratio
        INTEGER    :: my_group_pe_id

        ! If I am the last processor in the outer region
        ! set R_bndry_hi(x_last+1) = zero
        ! and R_bndry_hi(x_last+2) = 0.0_wp * psi_outer(x_last)

        ! Hugo has changed 0.0_wp to -1.0_wp as he does not understand this
        outer_boundary_ratio = -1.0_wp
        CALL get_my_group_pe_id(my_group_pe_id)
        IF (my_group_pe_id .EQ. pe_id_last_outer) THEN
            R_bndry_hi(x_last + 1, :, :) = zero
            R_bndry_hi(x_last + 2, :, :) = outer_boundary_ratio*psi_outer(x_last, :, :)
        END IF

    END SUBROUTINE get_missing_1st_deriv_bndries

!---------------------------------------------------------------------

    !> \brief Multiply the hamiltonian by the wavefunction (outer region)
    !>
    !> The multiplication to be performed is
    !> \f[ H\psi = \left( -\frac{1}{2} \frac{d^2}{dr^2} + \frac{l(l+1)}{2r^2} -
    !> \frac{(Z-N)}{r} \right) \psi  + \sum _{\gamma} (W_E +
    !> W_P + W_D) \psi_{\gamma} \f]
    !> where the sum is performed over all the channels \f$ \gamma \f$.
    !> Each term is computed in turn as required for a given calculation.
    !>
    !> The surface term in the TDSE needed for propagating the inner region coefficients looks like:
    !>  \f[ \frac{1}{2} \sum _{\gamma } \omega _{k,\gamma} \bar{f}_{\gamma}(b,t)
    !>  \f]
    !>  where
    !> \f[ \bar{f}_{\gamma}(b,t) = \left[ \frac{d}{dr} - \frac{\beta}{r}\right]
    !> f_{\gamma} + g\left(\frac{A(t)}{c}\right) \sum_{\gamma'} K_{\gamma,\gamma'} f_{\gamma'}\f]
    !> evaluated at \f$r=b\f$
    !>  where \f$ \beta\f$ is an arbitrary parameter and \f$ g=0 \f$ in length gauge
    !>  and \f$ g=1 \f$ in velocity gauge.
    !>
    !> Regardless of gauge and \f$ \beta \f$ we will always need the first derivative
    !> of f.
    !> Therefore we'll calculate it at the start of this subroutine and save it.
    !>
    !> NB this derivative is also needed for the laser-electron interaction in velocity gauge
    !> so we'll pass the derivatives to the incr_w_WP_Ham_x_Vec_out_vel subroutine
    !>
    !> Also in this routine- calculate \f$ \mathbf d | \psi \rangle \f$ and \f$
    !> \dot{\mathbf{d}} | \psi \rangle \f$, i.e. multiply the dipole and dipole
    !> velocity operators by the wavefunction with a view to evaluating their
    !> expectation value later on.
    !>
    !>
    !> To handle the several different cases (for which potentials to apply,
    !> which multiplications to perform etc.) this routine uses a number of
    !> logical flags. `get_expec` advises if the dipole expectation values are
    !> required (only calculated on the first call of each iteration). The value
    !> of `field_strength` is used to determine if the field coupling potential
    !> needs to be applied (.false. if field_strength = 0).
    SUBROUTINE ham_x_vector_outer(delR, Z_minus_N, field_strength, psi_outer, &
                                      h_psi_outer, R_bndry_lo, R_bndry_hi, &
                                      first_deriv_of_psi_at_b, i_am_pe0_outer, &
                                      WE_store, &
                                      expec_psi_outer, &
                                      get_expec)

        USE initial_conditions, ONLY: dipole_velocity_output
        USE lrpots,             ONLY: we_size

        REAL(wp), INTENT(IN)           :: delR, Z_minus_N
        REAL(wp), INTENT(IN)           :: field_strength(3, numsols)
        COMPLEX(wp), INTENT(IN)        :: psi_outer(x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(OUT)       :: h_psi_outer(x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(INOUT)     :: R_bndry_lo(x_1st - 2:x_1st - 1, channel_id_1st:channel_id_last, 1:numsols)
        COMPLEX(wp), INTENT(INOUT)     :: R_bndry_hi(x_last + 1:x_last + 2, channel_id_1st:channel_id_last, 1:numsols)
        REAL(wp), INTENT(INOUT)        :: WE_store(x_1st:x_last, we_size, channel_id_1st:channel_id_last)
        COMPLEX(wp), INTENT(OUT)       :: first_deriv_of_psi_at_b(channel_id_1st:channel_id_last, 1:numsols)
        LOGICAL, INTENT(IN)            :: i_am_pe0_outer
        LOGICAL, INTENT(IN)            :: get_expec

        COMPLEX(wp), ALLOCATABLE, INTENT(INOUT) :: expec_psi_outer(:,:,:,:,:) ! x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols, 1:3, 1:2

        INTEGER                        :: channel_id, little_L, parity, total_L, total_ML, isol
        COMPLEX(wp)                    :: deriv_psi(x_1st:x_last)
        COMPLEX(wp)                    :: first_derivatives_of_psi(x_1st:x_last, channel_id_1st:channel_id_last, 1:numsols)
        LOGICAL                        :: field_on
        REAL(wp)                       :: z_minus_n_over_r(x_1st:x_last)
        INTEGER                        :: number_grid_points

        ! In the molecular case the array ltuu keeps the values of the irreducible
        ! representations (not angular momentum) so at any point in this module
        ! where ltuu is used L must be changed (in particular occurences of ``L+1").

        ! Get updated R_bndry_hi to reflect outer_boundary_ratio = 0.0 for 2nd deriv at the outer boundary

        CALL get_missing_1st_deriv_bndries(psi_outer, R_bndry_hi)

        ! Check if the field is on or not
        IF (ALL(field_strength == 0.0_wp)) THEN
            field_on = .false.
        ELSE
            field_on = .true.
        END IF

        ! Calculate first derivatives: For surface term calculation and laser-electron vel gauge interaction
        first_deriv_of_psi_at_b = (0.0_wp, 0.0_wp)
        IF (dipole_velocity_output) THEN  !save all derivatives - all PEs must do this
            DO isol = 1, numsols
                !$OMP PARALLEL DO PRIVATE(channel_id, deriv_psi)
                DO channel_id = channel_id_1st, channel_id_last
                    CALL get4_1st_deriv &
                        (psi_outer(:, channel_id, isol), &
                        deriv_psi, &
                        R_bndry_lo(:, channel_id, isol), &
                        R_bndry_hi(:, channel_id, isol), &
                        one_over_delR)
                    first_derivatives_of_psi(:, channel_id, isol) = deriv_psi
                    first_deriv_of_psi_at_b(channel_id, isol) = deriv_psi(x_1st) !
                END DO
            END DO
        ELSE  ! only save derivative at inner boundary - only PE0 can calculate this
            IF (i_am_pe0_outer) THEN
                DO isol = 1, numsols
                    !$OMP PARALLEL DO PRIVATE(channel_id)
                    DO channel_id = channel_id_1st, channel_id_last
                        CALL get4_1st_deriv_at_b &
                            (psi_outer(x_1st + 1:x_1st + 2, channel_id, isol), &
                            first_deriv_of_psi_at_b(channel_id, isol), &
                            R_bndry_lo(:, channel_id, isol), &
                            one_over_delR)
                    END DO
                END DO
            END IF
        END IF

        ! Get updated R_bndry_hi to reflect outer_boundary_ratio = -1.0 for 2nd deriv at the outer boundary
        CALL get_missing_2nd_deriv_bndries(psi_outer, R_bndry_hi)

        IF (get_expec) THEN
            expec_psi_outer = (0.0_wp, 0.0_wp)
        END IF

        z_minus_n_over_r = Z_minus_N * Rinverse
        number_grid_points = x_last - x_1st + 1

        !$OMP PARALLEL DO PRIVATE(channel_id, little_L, parity, total_L, total_ML, isol) COLLAPSE(2)
        DO isol = 1, numsols
            DO channel_id = channel_id_1st, channel_id_last

                little_L = liuu(channel_id)
                parity = lpuu(channel_id)
                total_L = ltuu(channel_id)
                total_ML = lmuu(channel_id)  ! DDAC ADDED THIS

                ! Initialize h_psi_outer = - ( 1/2 grad^2 ) psi
                CALL init_w_atomicham_x_vector_outer &
                    (psi_outer(:, channel_id, isol), &
                    h_psi_outer(:, channel_id, isol), &
                    little_L, &
                    R_bndry_lo(:, channel_id, isol), &
                    R_bndry_hi(:, channel_id, isol), &
                    delR)

                ! - ( (Z-N)/r + 1/2 k^2 ) psi
                CALL apply_centrifugal_and_energy_shift &
                    (number_grid_points,&
                    psi_outer(:,channel_id, isol),&
                    h_psi_outer(:,channel_id, isol),&
                    z_minus_n_over_r,&
                    ksq(channel_id))

                CALL apply_long_range_potential_matrices &
                    (psi_outer(:, :, isol), &
                    h_psi_outer(:,channel_id, isol), &
                    total_ML, &
                    WE_store, &
                    channel_id, &
                    number_grid_points, &
                    field_strength(:, isol), &
                    RR,&
                    field_on)

                IF (get_expec) THEN ! This routine is only called for order 0 of the propagation
                    CALL get_Z_psi_outer &
                        (psi_outer(:, :, isol), &
                            expec_psi_outer(:, channel_id, isol, :, :), &
                            total_ML, &
                            channel_id, &
                            first_derivatives_of_psi(:, :, isol))
                END IF

            END DO
        END DO

    END SUBROUTINE ham_x_vector_outer

!------------------------------------------------------------------
    ! h_psi_outer = h_psi_outer + V' psi'
    ! V' = W_E + W_P + W_D
    ! W_E = Coulomb interaction
    ! W_D = long range potential coupling together channels in outer region via inner target
    !       states through laser interaction
    ! W_P = laser coupling to outer electron
    SUBROUTINE apply_long_range_potential_matrices (psi, &
                                                    h_psi, &
                                                    total_ML, &
                                                    WE_store, &
                                                    channel_id, &
                                                    number_grid_points, &
                                                    field_strength, &
                                                    r_store,&
                                                    field_on)

        USE calculation_parameters, ONLY: use_we_ham_interactions, &
                                          use_wd_ham_interactions, &
                                          use_wp_ham_interactions
        USE lrpots,                 ONLY: we_size

        INTEGER, INTENT(IN) :: number_grid_points
        INTEGER, INTENT(IN) :: total_ML
        INTEGER, INTENT(IN) :: channel_id
        REAL(wp), INTENT(IN):: WE_store(number_grid_points, we_size, channel_id_1st:channel_id_last)
        REAL(wp), INTENT(IN):: field_strength(3)
        REAL(wp), INTENT(IN):: r_store(number_grid_points)
        COMPLEX(wp), INTENT(IN):: psi(number_grid_points, channel_id_1st : channel_id_last)
        COMPLEX(wp), INTENT(INOUT):: H_psi(number_grid_points, channel_id_1st : channel_id_last)
        LOGICAL, INTENT(IN) :: field_on

        ! h_psi= h_psi+ sum_channel'gamma' W_E psi_channel'gamma'
        IF (use_we_ham_interactions) THEN
            CALL incr_w_we_ham_x_vector_outer &
                (psi, &
                 H_psi, &
                 WE_store, &
                 channel_id,&
                 number_grid_points)
        END IF

        IF (field_on) THEN
            ! h_psi= h_psi+ sum_channel'gamma' W_D psi_channel'gamma'
            IF (use_wd_ham_interactions) THEN
                CALL incr_w_wd_ham_x_vector_outer &
                    (psi, &
                     H_psi, &
                     total_ML, &
                     channel_id, &
                     field_strength,&
                     number_grid_points)
            END IF

            ! h_psi= h_psi+ sum_channel'gamma' W_P psi_channel'gamma'
            IF (use_wp_ham_interactions) THEN
                CALL incr_w_wp_ham_x_vec_out_len &
                    (psi, &
                     H_psi, &
                     total_ML, &
                     channel_id, &
                     field_strength,&
                     number_grid_points,&
                     r_store)
            END IF
        END IF

    END SUBROUTINE apply_long_range_potential_matrices


    SUBROUTINE init_w_atomicham_x_vector_outer(psi, H_psi, L, R_bndry_lo, R_bndry_hi, delR)

        USE local_ham_matrix, ONLY: INCR_with_laplacian
        ! DDAC: Modified

        COMPLEX(wp), INTENT(IN)  :: psi(x_1st:x_last)
        COMPLEX(wp), INTENT(OUT) :: H_psi(x_1st:x_last)
        INTEGER, INTENT(IN)      :: L
        COMPLEX(wp), INTENT(IN)  :: R_bndry_lo(x_1st - 2:x_1st - 1)
        COMPLEX(wp), INTENT(IN)  :: R_bndry_hi(x_last + 1:x_last + 2)
        REAL(wp), INTENT(IN)     :: delR

        REAL(wp) second_deriv_coeff
        REAL(wp) kinetic_E_coeff, atomic_ham_factor
        !EX!

        kinetic_E_coeff = 1.0_wp/(delR*delR)    !! due to finite difference rule for d2/dr2
        atomic_ham_factor = -0.5_wp                     !! since Hamiltonian has -1/2 grad^2

        second_deriv_coeff = kinetic_E_coeff*atomic_ham_factor

        H_psi = (0.0_wp, 0.0_wp)

        CALL INCR_with_laplacian &
            (psi, &
             H_psi, &
             L, &
             second_deriv_coeff, &
             atomic_ham_factor, &
             Rinverse2, &
             R_Bndry_Lo, &
             R_bndry_hi)

    END SUBROUTINE init_w_atomicham_x_vector_outer


    SUBROUTINE apply_centrifugal_and_energy_shift (number_of_grid_points, psi, H_psi, Z_minus_N_over_R, k2)
        INTEGER, INTENT(IN)        :: number_of_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_of_grid_points)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_of_grid_points)
        REAL(wp), INTENT(IN)       :: Z_minus_N_over_R(number_of_grid_points)
        COMPLEX(wp), INTENT(IN)    :: k2

        H_psi = H_psi - ( Z_minus_N_over_R + 0.5_wp * k2 ) *  psi

    END SUBROUTINE apply_centrifugal_and_energy_shift
!---------------------------------------------------------------------

    !> \brief   Apply WE potential on vector
    !> \authors G Armstrong, A Brown
    !> \date    2018
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f $W_E$ \f$ potential
    !> and adds the result to \c H_Psi (corresponding to channel \c j). The multiplication is only
    !> carried out if the targets in the two channels are coupled by the dipole operator. Note that
    !> the check for coupling is not performed here, but rather only those channels which are coupled
    !> by \f$ W_E \f$ are listed in the vector `we_neighbour_store` and these are the channels which
    !> are used
    SUBROUTINE incr_w_we_ham_x_vector_outer(psi, H_psi, WE_store, channel_i,number_grid_points)

        USE lrpots,     ONLY: we_size

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(IN)    :: psi(number_grid_points, channel_id_1st:channel_id_last)
        COMPLEX(wp), INTENT(INOUT) :: H_psi(number_grid_points)
        REAL(wp), INTENT(IN)       :: WE_store(number_grid_points, we_size, channel_id_1st:channel_id_last)
        INTEGER, INTENT(IN)        :: channel_i
        INTEGER                    :: channel_j
        INTEGER                    :: ii

        DO ii = 1,we_neighbour_count(channel_i)
            channel_j = we_neighbour_store(channel_i,ii)
            H_psi(:) = H_psi(:) + WE_store(:,ii,channel_i)*psi(:, channel_j)
        END DO

    END SUBROUTINE incr_w_we_ham_x_vector_outer

!---------------------------------------------------------------------

    !> \brief Apply WD potential on vector (outer)
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f$ W_D \f$ potential
    !> and adds the result to \c H_Psi (corresponding to channel \c j). The multiplication is only
    !> carried out if the targets in the two channels are coupled by the dipole operator. Note that
    !> the check for coupling is not performed here, but rather only those channels which are coupled
    !> by \f$ W_D \f$ are listed in the vector `wd_neighbour_store` and these are the channels which
    !> are used.
    SUBROUTINE incr_w_wd_ham_x_vector_outer(psi, H_psi, total_MLi, channel_i, field_strength, number_grid_points)

        USE coupling_rules, ONLY: field_contraction
        USE potentials,     ONLY: makewdpmat

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, channel_id_1st:channel_id_last)
        INTEGER, INTENT(IN)        :: total_MLi, channel_i
        REAL(wp), INTENT(IN)       :: field_strength(3)

        INTEGER     :: channel_j, total_MLj, ii
        COMPLEX(wp) :: WD_Ham(3), WD_Ham_Coeff

        DO ii = 1,wd_neighbour_count(channel_i)
            channel_j = wd_neighbour_store(channel_i,ii)
            total_MLj = lmuu(channel_j)

            CALL makewdpmat(channel_i, channel_j, wd_ham)

            wd_ham_coeff = field_contraction(field_strength, wd_ham, total_MLj, total_MLi)

            IF (wd_ham_coeff /= 0) THEN
                H_psi(:) = H_psi(:) + wd_ham_coeff*psi(:, channel_j)
            END IF
        END DO

    END SUBROUTINE incr_w_wd_ham_x_vector_outer

!---------------------------------------------------------------------

    !> \brief Apply WD potential (velocity gauge) on vector (outer)
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f$ W_D \f$ potential
    !> and adds the result to \c H_Psi (corresponding to channel \c j). The multiplication is only
    !> carried out if the targets in the two channels are coupled by the dipole operator. Note that
    !> the check for coupling is not performed here, but rather only those channels which are coupled
    !> by \f$ W_D \f$ are listed in the vector `wd_neighbour_store` and these are the channels which
    !> are used.
    SUBROUTINE incr_w_wd_ham_x_vector_outer_vel(psi, H_psi, total_MLi, channel_i,&
                                            field_strength, number_grid_points)

        USE coupling_rules, ONLY: field_contraction
        USE potentials,     ONLY: makewdpmat_v

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, channel_id_1st:channel_id_last)
        INTEGER, INTENT(IN)        :: total_MLi, channel_i
        REAL(wp), INTENT(IN)       :: field_strength(3)

        INTEGER     :: channel_j, total_MLj, ii
        COMPLEX(wp) :: WD_Ham(3), WD_Ham_Coeff

        DO ii = 1,wd_neighbour_count(channel_i)
            channel_j = wd_neighbour_store(channel_i,ii)
            total_MLj = lmuu(channel_j)

            CALL makewdpmat_v(channel_i, channel_j, wd_ham)

            wd_ham_coeff = field_contraction(field_strength, wd_ham, total_MLj, total_MLi)

            IF (wd_ham_coeff /= 0) THEN
                H_psi(:) = H_psi(:) + wd_ham_coeff*psi(:, channel_j)
            END IF
        END DO

    END SUBROUTINE incr_w_wd_ham_x_vector_outer_vel

!---------------------------------------------------------------------

    SUBROUTINE incr_w_wp_ham_x_vec_out_vel(psi, H_psi, total_MLi, channel_i,&
                                           field_strength, number_grid_points, first_derivs_of_psi)  ! Atomic

        ! DDAC: Modified
        USE coupling_rules, ONLY: field_contraction
        USE potentials,     ONLY : makewpmat, makewpmat2_v

        INTEGER,    INTENT(IN)     :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points,channel_id_1st:channel_id_last)
        INTEGER, INTENT(IN)        :: channel_i, total_MLi
        REAL(wp), INTENT(IN)       :: field_strength(3)

        COMPLEX(wp), INTENT(IN)    :: first_derivs_of_psi(1:number_grid_points, channel_id_1st:channel_id_last)

        INTEGER                    :: channel_j, total_MLj, ii
        COMPLEX(wp)                :: wp_ham1(3), wp_ham2(3)
        COMPLEX(wp)                :: wp_ham_coeff1,wp_ham_coeff2

        DO ii = 1, wp_neighbour_count(channel_i)
            channel_j = wp_neighbour_store(channel_i,ii)
            total_MLj = lmuu(channel_j)

            CALL makewpmat(channel_i, channel_j, wp_ham1)
            CALL makewpmat2_v(channel_i, channel_j, wp_ham2)

            wp_ham_coeff1 = field_contraction(field_strength(:), wp_ham1, total_MLj, total_MLi)
            wp_ham_coeff2 = field_contraction(field_strength(:), wp_ham2, total_MLj, total_MLi)

            IF ( (wp_ham_coeff1 /= 0) .OR. (wp_ham_coeff2 /=0) ) THEN
                H_psi(:) = H_psi(:) + &
                        (wp_ham_coeff1*first_derivs_of_psi(:, channel_j) + &
                            wp_ham_coeff2*Rinverse(:)*psi(:, channel_j))
            END IF
        END DO

    END SUBROUTINE incr_w_wp_ham_x_vec_out_vel

!---------------------------------------------------------------------

    !> \brief Apply WP potential on vector (outer)
    !>
    !> Multiplies the vector \c Psi (corresponding to channel \c i) by the \f$ W_P \f$ potential
    !> in length gauge and adds the result to \c H_Psi (corresponding to channel \c j). The multiplication is only
    !> carried out if the porjectiles in the two channels are coupled by the dipole operator. Note that
    !> the check for coupling is not performed here, but rather only those channels which are coupled
    !> by \f$ W_P \f$ are listed in the vector `wp_neighbour_store` and these are the channels which
    !> are used
    SUBROUTINE incr_w_wp_ham_x_vec_out_len(psi, H_psi, total_MLi, channel_i, field_strength, number_grid_points, r_store)

        USE coupling_rules, ONLY: field_contraction
        USE potentials,     ONLY: makewpmat2

        INTEGER, INTENT(IN)        :: number_grid_points
        COMPLEX(wp), INTENT(INOUT) :: H_psi(1:number_grid_points)
        COMPLEX(wp), INTENT(IN)    :: psi(1:number_grid_points, channel_id_1st:channel_id_last)
        INTEGER, INTENT(IN)        :: channel_i, total_MLi
        REAL(wp), INTENT(IN)       :: field_strength(3)
        REAL(wp), INTENT(IN)       :: r_store(number_grid_points)

        INTEGER     :: channel_j, total_MLj, ii
        COMPLEX(wp) :: wp_ham2(3), wp_ham2_coeff

        DO ii = 1, wp_neighbour_count(channel_i)
            channel_j = wp_neighbour_store(channel_i,ii)
            total_MLj = lmuu(channel_j)

            CALL makewpmat2(channel_i, channel_j, wp_ham2)

            wp_ham2_coeff = field_contraction(field_strength, wp_ham2, total_MLj, total_MLi)

            IF (wp_ham2_coeff /= 0) THEN
                H_psi(:) = H_psi(:) + wp_ham2_coeff*r_store(:)*psi(:, channel_j)  ! WP Ham has form WP_Ham2 * r
            END IF
        END DO

    END SUBROUTINE incr_w_wp_ham_x_vec_out_len

!---------------------------------------------------------------------

    !> \brief    Apply dipole operator on outer wave function
    !> \authors  A Brown, D Clarke, J Benda, J Wragg
    !> \date     2018 - 2019
    !>
    !> Calculates the product of the dipole operator (currently z-component) and the wave functions in the outer
    !> region. Optionally, if Dipole_Velocity_Output is on, it also evaluates the time derivative of the product.
    !>
    !> \note DDAC (06/2018): Modified for calculation of the z-component of the dipole moment
    !>
    SUBROUTINE get_Z_psi_outer(psi, expec_psi, total_MLi, channel_i, first_derivs_of_psi)

        USE initial_conditions,        ONLY: dipole_velocity_output,        &
                                             dipole_dimensions_desired,     &
                                             length_guage_id,               &
                                             velocity_guage_id
        USE local_ham_matrix,          ONLY: windowed_RR

        COMPLEX(wp), INTENT(IN)    :: psi(x_1st:x_last, channel_id_1st:channel_id_last)
        COMPLEX(wp), INTENT(INOUT) :: expec_psi(x_1st:x_last,1:3,1:2)
        INTEGER, INTENT(IN)        :: channel_i, total_MLi
        COMPLEX(wp), INTENT(IN)    :: first_derivs_of_psi(x_1st:x_last, channel_id_1st:channel_id_last)

        REAL(wp)                   :: proj(3)
        INTEGER :: Dimension_Counter

        ! Set contraction factor to project WP_Ham2 and WD_Ham2 onto the requested axis, currently z-axis.
        ! Some day, this may be generalized also to other axes. The minus sign compensates for electron charge
        ! in the dipole operator.

        DO Dimension_Counter=1,3
            IF (dipole_dimensions_desired(Dimension_Counter)) THEN

                proj = (/ 0, 0, 0/)
                proj(Dimension_Counter)=-1

                CALL incr_w_wp_ham_x_vec_out_len (psi, expec_psi(:,Dimension_Counter,length_guage_id), &
                                                total_MLi, channel_i,  proj,&
                                                x_last-x_1st+1, windowed_RR )

                CALL incr_w_wd_ham_x_vector_outer(psi, expec_psi(:,Dimension_Counter,length_guage_id), &
                                                total_MLi, channel_i, proj,&
                                                x_last-x_1st+1)

                IF (dipole_velocity_output) THEN

                    CALL incr_w_wp_ham_x_vec_out_vel(psi, expec_psi(:,Dimension_Counter,velocity_guage_id), &
                                                    total_MLi, channel_i, proj, &
                                                    x_last-x_1st+1, first_derivs_of_psi)

                    CALL incr_w_wd_ham_x_vector_outer_vel(psi, expec_psi(:,Dimension_Counter,velocity_guage_id), &
                                                        total_MLi, channel_i, proj, &
                                                        x_last-x_1st+1)
                END IF

            END IF

        END DO

    END SUBROUTINE get_Z_psi_outer

!---------------------------------------------------------------------

    !> \brief Test relations between coupling matrix elements for long range potentials
    !>
    !> Checks that all non-zero elements of the long range potentials between individual channels
    !> conform to the expected coupling rules. Also, counts number of couplings for each channel
    !> (i.e. number of channels j coupled to channel i by \f$ W_E, W_D\f$ and \f$ W_P\f$ ). The
    !> channels j which couple to channel i via \f$ W_E \f$ are stored in `we_neighbour_store(i,:)`
    !> and the number of such couplings is stored in `we_neighbour_count` (same for wp and wd). This
    !> allows all couplings to be evaluated before the time loop and stored, to avoid testing for
    !> coupling repeatedly.
    !>
    SUBROUTINE test_outer_hamiltonian(i_can_do_io)
        USE lrpots,         ONLY: we_size, wp_size, wd_size
        USE coupling_rules, ONLY: target_coupled
        USE initial_conditions, ONLY : debug, dipole_velocity_output

        LOGICAL, INTENT(IN) :: i_can_do_io

        COMPLEX(wp) :: wp_ham1(3), wp_ham2(3), wd_ham(3)
        REAL(wp)    :: we_ham, nonzero = 1.0e-14_wp
        INTEGER     :: channel_i, channel_j, tgt_i, tgt_j, little_li, little_lj, little_mi, little_mj
        INTEGER     :: total_Si, total_Sj, parityi, parityj, total_Li, total_Lj, total_MLi, total_MLj

        CHARACTER(110) :: tmplt_i, tmplt_j
        INTEGER :: coupling_index

        CALL allocate_coupling_matrices

        tmplt_i = '(" Channel ",I5,": Si = ",I3,", Li = ",I3,", MLi = ",I3,", Pi = ",I3,", Ti = ",I3,", li = ",I3,", mi = ",I3)'
        tmplt_j = '(" Channel ",I5,": Sj = ",I3,", Lj = ",I3,", MLj = ",I3,", Pj = ",I3,", Ti = ",I3,", lj = ",I3,", mj = ",I3)'

        IF (i_can_do_IO .AND. debug) WRITE (*, '(/,1x,A)') 'Test outer Hamiltonian'

        ! WE
        DO channel_i = channel_id_1st, channel_id_last
            coupling_index = 0
            total_Si = lsuu(channel_i); total_Li = ltuu(channel_i); total_MLi = lmuu(channel_i)
            parityi = lpuu(channel_i); little_li = liuu(channel_i); little_mi = miuu(channel_i); tgt_i = icuu(channel_i)

            DO channel_j = channel_id_1st, channel_id_last
                total_Sj = lsuu(channel_j); total_Lj = ltuu(channel_j); total_MLj = lmuu(channel_j)
                parityj = lpuu(channel_j); little_lj = liuu(channel_j); little_mj = miuu(channel_j); tgt_j = icuu(channel_j)

                IF (abs(channel_i - channel_j) <= we_size ) THEN
                    CALL makewemat(RR(x_1st), channel_i, channel_j, WE_ham)
                ELSE
                    WE_ham = 0.0_wp
                END IF

                IF  (ABS(WE_ham) > nonzero) THEN
                    ! WE coupling requires equal total quantum numbers
                    IF (total_Li == total_Lj .AND. total_MLi == total_MLj .AND. parityi == parityj) THEN
                        coupling_index = coupling_index +1
                        we_neighbour_count(channel_i) = coupling_index
                        we_neighbour_store(channel_i,coupling_index) = channel_j
                    END IF
                END IF
            END DO
            END DO

        ! WP
        DO channel_i = channel_id_1st, channel_id_last
            coupling_index = 0
            total_Si = lsuu(channel_i); total_Li = ltuu(channel_i); total_MLi = lmuu(channel_i)
            parityi = lpuu(channel_i); little_li = liuu(channel_i); little_mi = miuu(channel_i); tgt_i = icuu(channel_i)

            DO channel_j = channel_id_1st, channel_id_last
                total_Sj = lsuu(channel_j); total_Lj = ltuu(channel_j); total_MLj = lmuu(channel_j)
                parityj = lpuu(channel_j); little_lj = liuu(channel_j); little_mj = miuu(channel_j); tgt_j = icuu(channel_j)

                IF (ABS(channel_i - channel_j) <= wp_size) THEN
                    IF (dipole_velocity_output) THEN
                        CALL makewpmat(channel_i, channel_j, wp_ham1)
                    ELSE
                        wp_ham1 = 0.0_wp
                    END IF
                    CALL makewpmat2(channel_i, channel_j, wp_ham2)
                ELSE
                    wp_ham1 = zero
                    wp_ham2 = zero
                END IF

                IF ( any(ABS(wp_ham1) > nonzero) .or. any(ABS(wp_ham2) > nonzero) ) THEN
                    ! WP coupling requires dipole coupled targets and projectiles, and equal targets
                    IF (projectile_lcoupled(channel_i, channel_j) .AND. tgt_i == tgt_j) THEN
                        coupling_index = coupling_index + 1
                        wp_neighbour_count(channel_i) = coupling_index
                        wp_neighbour_store(channel_i,coupling_index) = channel_j
                    END IF
                END IF
            END DO
        END DO

        ! WD
        DO channel_i = channel_id_1st, channel_id_last
           coupling_index = 0
            total_Si = lsuu(channel_i); total_Li = ltuu(channel_i); total_MLi = lmuu(channel_i)
            parityi = lpuu(channel_i); little_li = liuu(channel_i); little_mi = miuu(channel_i); tgt_i = icuu(channel_i)

            DO channel_j = channel_id_1st, channel_id_last
                total_Sj = lsuu(channel_j); total_Lj = ltuu(channel_j); total_MLj = lmuu(channel_j)
                parityj = lpuu(channel_j); little_lj = liuu(channel_j); little_mj = miuu(channel_j); tgt_j = icuu(channel_j)

                IF (ABS(channel_i - channel_j) <= wd_size) THEN
                    CALL makewdpmat(channel_i, channel_j, wd_ham)
                ELSE
                    wd_ham = zero
                END IF

                IF (any(ABS(wd_ham) > nonzero)) THEN
                    ! WD coupling requires dipoled coupled (total) systems and equal channels
                    IF (target_coupled(channel_i, channel_j) .AND. little_li == little_lj .AND. little_mi == little_mj) THEN
                        coupling_index = coupling_index + 1
                        wd_neighbour_count(channel_i) = coupling_index
                        wd_neighbour_store(channel_i,coupling_index) = channel_j
                    END IF
                END IF
            END DO
        END DO

        ! Print number of neighbours
        IF (i_can_do_io .AND. debug) THEN
            PRINT *, 'Number of neighbours:'
            DO channel_i = channel_id_1st, channel_id_last
                total_Si = lsuu(channel_i); total_Li = ltuu(channel_i); total_MLi = lmuu(channel_i)
                parityi = lpuu(channel_i); little_li = liuu(channel_i); little_mi = miuu(channel_i); tgt_i = icuu(channel_i)

                WRITE (*, tmplt_i, advance='no') &
                    channel_i, total_Si, total_Li, total_MLi, parityi, tgt_i, little_li, little_mi
                WRITE (*, '(": #WE = ",I4,", #WP = ",I4,", #WD = ",I4)') &
                    we_neighbour_count(channel_i), wp_neighbour_count(channel_i), wd_neighbour_count(channel_i)
            END DO
        END IF

        ! Reset sizes to actual width of the packed storage rather than the half-band estimate
        wd_size = MAXVAL(wd_neighbour_count)
        we_size = MAXVAL(we_neighbour_count)
        wp_size = MAXVAL(wp_neighbour_count)

    END SUBROUTINE test_outer_hamiltonian


    !> \brief Allocate the arrays for the coupling information
    !>
    !> For each channel i, we will store the channels j to which i couples via
    !> the \f$ W_D, W_E \f$ and \f$ W_P\f$ matrices.
    SUBROUTINE allocate_coupling_matrices
        USE lrpots, ONLY: we_size, wp_size, wd_size
        USE grid_parameters, ONLY: channel_id_1st, &
                                   channel_id_last
        IMPLICIT NONE
        INTEGER :: ierr

        allocate(wp_neighbour_store(channel_id_1st:channel_id_last, 2*wp_size + 1),&
                 wd_neighbour_store(channel_id_1st:channel_id_last, 2*wd_size + 1),&
                 we_neighbour_store(channel_id_1st:channel_id_last, 2*we_size + 1),&
                 wp_neighbour_count(channel_id_1st:channel_id_last),&
                 wd_neighbour_count(channel_id_1st:channel_id_last),&
                 we_neighbour_count(channel_id_1st:channel_id_last),&
                 stat=ierr)

        CALL assert((ierr == 0), 'allocation error in outer_hamiltonian')

        ! Initialize
        WP_neighbour_count = 0  ;  WP_neighbour_store = (0,0)
        WE_neighbour_count = 0  ;  WE_neighbour_store = (0,0)
        WD_neighbour_count = 0  ;  WD_neighbour_store = (0,0)

    END SUBROUTINE allocate_coupling_matrices


    !> \brief Setup and store the WE array
    !> \authors  A Brown
    !>
    !> The array storing the WE potential coefficients  (coupling the electron to the residual
    !> ion) is set up outside of the time loop to avoid unnecessary steps in the
    !> time propagation.
    SUBROUTINE setup_WE_store (WE_store, r_store, number_grid_points)
        USE lrpots,     ONLY: we_size
        USE potentials, ONLY: makewemat
        INTEGER,  INTENT(IN)    :: number_grid_points
        REAL(wp), INTENT(INOUT) :: WE_store(number_grid_points, we_size, channel_id_1st:channel_id_last)
        REAL(wp), INTENT(IN)    :: r_store(number_grid_points)
        INTEGER                  :: channel_i, channel_j
        INTEGER                  :: ii, i
        REAL(wp)                 :: WE_ham

        DO channel_i = channel_id_1st, channel_id_last

            DO ii = 1, we_neighbour_count(channel_i)
                channel_j = we_neighbour_store(channel_i,ii)

                DO i = 1, number_grid_points
                    CALL makewemat(r_store(i), channel_i, channel_j, WE_ham)
                    WE_store(i,ii,channel_i) = WE_ham
                END DO
            END DO
        END DO

    END SUBROUTINE setup_WE_store

END MODULE outer_hamiltonian
